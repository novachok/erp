var express    = require('express');
var bodyParser = require('body-parser');
var path       = require('path');
var swig       = require('swig');

var config   = require('./sys/config');
var db       = require('./sys/db');
var _        = require('./lib/_');
var security = require('./sys/security');

var app = express();
app.use(express.static('public'));
app.engine('html', swig.renderFile);
app.set('view engine', 'html');
app.set('views', __dirname + '/public');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));


app.use('/api', require('./api/api'));

app.get('/reset/:code', function(req, res, next) {
    var user = db.getModel('user');
    user.findOne({
            where: {
                resetHash: req.params.code
            }
        })
        .then(function(user) {
            if(user) {
                return res.render('reset', {user: user});
            } else {
                return res.redirect('/');
            }
        })
        .catch(function() {
            return res.redirect('/');
        });
});

app.post('/reset/:code', function(req, res, next) {
    var user = db.getModel('user');
    user.findOne({
            where: {
                resetHash: req.params.code
            }
        })
        .then(function(user) {
            if(user && req.body.pwd && req.body.pwdConfirm && req.body.pwd == req.body.pwdConfirm) {

                user.password = security.sha256(req.body.pwd, user.salt);
                return user.save()
                    .then(function(){
                        res.redirect('/');
                    })
                    .catch(function(){
                        res.redirect('/');
                    })
            } else {
                res.redirect('/');
            }
        })
        .catch(function() {
            res.redirect('/');
        });
});

app.use('/error/:code', function(req, res, next) {
    var errors = db.getModel('error');
    errors.findOne({
            where: {code: {$in: [1000, req.params.code]}},
            order: 'code DESC'
        })
        .then(function(error) {
            if(error)
            {
                return res.render('error', {error: error});
            } else {
                return next();
            }
        })
        .catch(function(err) {
            return next(err);
        });
});

app.get('*', function(req, res, next){
    res.sendFile(path.join(__dirname, 'public', 'index.html'));
});

app.use(function(err, req, res, next){
    var code = err.toString().replace(/^\D+/g, '') * 1;
    if(_.isNumber(code) && code > 1000)
    {
        var errors = db.getModel('error');
        errors.findOne({
                where: {code: code}
            })
            .then(function(error){
                if(error)
                {
                    err.status  = error.status || 400;
                    err.code    = error.code;
                    err.message = error.title;
                    err.url = config.http.protocol +'://'+ config.http.address + (config.http.port ? ':'+ config.http.port : '') + '/';
                    err.url += 'error/'+ error.code;
                }
                return next(err);
            });
    } else
    {
        next(err);
    }
});

app.use(function(err, req, res, next){
    var response = {
        success: false,
        error: {
            code: err.code || null,
            message: err.message,
            url: err.url || null
        }
    };

    if(config.security.debug) {
        response.error.stack = err.stack;
    }

    res
        .status(err.status || 400)
        .json(response);
});


app.listen(config.http.port, function() {
    console.log('App has been started at port: '+ config.http.port);
    config.logger.log("info", "App has been started at port: [%s] with environment: [%s]", config.http.port, config.environment);
});