cp config.json.dist config.json

./bin/sequelize db:migrate --config ./sys/seq-config-proxy.js
./bin/sequelize db:seed:all --config ./sys/seq-config-proxy.js

bower install
gulp build
