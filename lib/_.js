var _ = require('underscore');


_.mixin({
    deepExtend: function(obj, ext) {
        if(_.isObject(obj)) {
            for (var prop in ext) {
                if (prop in obj && _.isObject(obj[prop])) {
                    _.deepExtend(obj[prop], ext[prop]);
                } else {
                    obj[prop] = ext[prop];
                }
            }
        }

        return obj;
    },
    capitalize: function(string) {
        return string.length > 0 ? string.charAt(0).toUpperCase() + string.slice(1) : string;
    },
    unique: function(length) {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_";

        for( var i=0; i < length; i++ )
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    },
    isNumeric: function(value) {
        if(_.isNumber(value)) {
            return true;
        } else if(_.isString(value)) {
            return /^-?\d+$/g.test(value);
        }
        return false;
    },
    setCookie: function(name, value, days) {
        var cookie = name + "=" + value;
        if(days)
        {
            days = days > 0 ? (days*24*60*60*1000) : -10000;
            var d = new Date();
            d.setTime(d.getTime() + days);
            var expires = "expires="+d.toUTCString();
            cookie += "; " + expires;
        }
        document.cookie = cookie;
    },
    getCookie: function(name) {
        var name = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0; i<ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0)==' ')
                c = c.substring(1);
            if (c.indexOf(name) == 0)
                return c.substring(name.length, c.length);
        }
        return "";
    },
    delCookie: function(name) {
        return this.setCookie(name, '', -1);
    }
});

module.exports = _;