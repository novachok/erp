var gulp = require('gulp');
var plug = require('gulp-load-plugins')({ lazy: true });
var browserify = require('browserify');
var babelify = require('babelify');
var babel = require('babel-core/register');
var source = require('vinyl-source-stream');
var minify = require('gulp-minify');

gulp.task('babelify', function(){
    return browserify({
            extensions: ['.jsx', '.js'],
            entries: './public/jsx/app.jsx'
        })
        .transform(babelify.configure({ ignore: /(node_modules)/ }))
        .bundle()
        .on("error", function (err) { console.log("Error : " + err.message); })
        .pipe(source('app.js'))
        .pipe(gulp.dest('./public/js'));
});

gulp.task('compress', function(){
    gulp.src('./public/js/*.js')
        .pipe(minify({
            ignoreFiles: ['-min.js']
        }))
        .pipe(gulp.dest('./public/js'))
});


gulp.task('build', ['babelify'], function(){

    gulp.src('./public/js/app.js')
        .pipe(minify({
            ext: '.min.js'
        }))
        .pipe(gulp.dest('./public/js'));

    return true;
});