'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    queryInterface.sequelize.query('TRUNCATE TABLE errors');
    return queryInterface.bulkInsert('errors', [
      {code: 1000, title: "Unknown error", description: "Unspecified error occurs."},
      {code: 1001, status: 401, title: "User doesn't exist or password is incorrect", description: "User doesn't exist or can't be found with provided credentials."},
      {code: 1002, status: 401, title: "Token is expired", description: "You have provided expired or invalid token."}
    ], {});
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('errors', null, {});
  }
};