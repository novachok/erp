'use strict';

var _   = require('../lib/_');
var sec = require('../sys/security');

module.exports = {
  up: function (queryInterface, Sequelize) {

    var salt = _.unique(32);

    return queryInterface.bulkInsert('users', [{
      email: 'novachok@gmail.com',
      firstName: 'Alexandr',
      lastName: 'Novachok',
      salt: salt,
      password: sec.sha256('helloworld', salt),
      createdAt: (function(){ return new Date(); })(),
      updatedAt: (function(){ return new Date(); })()
    }], {});
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('users', null, {});
  }
};
