var db = require('../sys/db');
var Customer = db.getModel('customer');

module.exports = function(seq, DataTypes) {

    var Hosting = seq.define("hosting", {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        createdAt: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        },
        updatedAt: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        },
        validTo: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        },
        domain: {
            type: DataTypes.STRING(128),
            allowNull: false,
            unique: true
        },
        account: {
            type: DataTypes.STRING(16),
            allowNull: false
        },
        onlyHosting: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        onlyDomain: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        space: {
            type: DataTypes.STRING(100)
        },
        notes: {
            type: DataTypes.TEXT
        }
    }, {
        tableName: 'hosting'
    });

    Hosting.belongsTo(Customer);

    return Hosting;
};