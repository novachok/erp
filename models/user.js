var db = require('../sys/db');
var BankAccount  = db.getModel('bankAccount');

module.exports = function(seq, DataTypes) {
    var User = seq.define("user", {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        createdAt: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        firstName: {
            type: DataTypes.STRING
        },
        lastName: {
            type: DataTypes.STRING
        },
        salt: {
            type: DataTypes.STRING
        },
        password: {
            type: DataTypes.STRING
        },
        title: {
            type: DataTypes.STRING
        },
        inn: {
            type: DataTypes.STRING(10)
        },
        phone: {
            type: DataTypes.STRING(20)
        },
        address: {
            type: DataTypes.STRING
        },
        resetHash: {
            type: DataTypes.STRING(14)
        }
    }, {
        tableName: 'users',
        getterMethods: {
            safeData: function(){
                var _u = {
                    id: this.id,
                    email: this.email,
                    firstName: this.firstName,
                    lastName: this.lastName,
                    title: this.title,
                    inn: this.inn,
                    phone: this.phone,
                    address: this.address,
                    accounts: []
                };

                if(this.accounts && this.accounts.length > 0) {
                    for(var x in this.accounts) {
                        _u.accounts.push({
                            id: this.accounts[x].id,
                            title: this.accounts[x].title,
                            account: this.accounts[x].account,
                            mfo: this.accounts[x].mfo,
                            isDefault: this.accounts[x].isDefault,
                            currency: this.accounts[x].currency
                        });
                    }
                }

                return _u;
            }
        }
    });

    User.hasMany(BankAccount, {as: 'accounts', foreignKey: 'userId'});

    return User;
};