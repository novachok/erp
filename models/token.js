var db = require('../sys/db');
var User = db.getModel('user');

module.exports = function(seq, DataTypes) {

    var Token = seq.define("token", {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        createdAt: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        },
        updatedAt: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        },
        token: {
            type: DataTypes.STRING(128),
            allowNull: false,
            unique: true
        }
    }, {
        tableName: 'tokens'
    });

    Token.belongsTo(User);

    return Token;
};