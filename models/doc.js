var db = require('../sys/db');
var Customer    = db.getModel('customer');
var DocItem     = db.getModel('docItem');
var BankAccount = db.getModel('bankAccount');

module.exports = function(seq, DataTypes) {

    var Doc = seq.define("doc", {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        createdAt: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        },
        updatedAt: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        },
        isInvoiced: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        dateOf: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        },
        isActed: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        actOf: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        },
        num: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false
        }
    }, {
        tableName: 'docs'
    });

    Doc.belongsTo(Customer);
    Doc.belongsTo(BankAccount, {foreignKey: 'accountId'});
    Doc.hasMany(DocItem, {as: 'items', foreignKey: 'docId'});

    return Doc;
};