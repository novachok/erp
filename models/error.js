module.exports = function(seq, DataTypes) {
    return seq.define("error", {
        id: {
            type: DataTypes.INTEGER.UNSIGNED,
            primaryKey: true,
            autoIncrement: true
        },
        code: {
            type: DataTypes.INTEGER.UNSIGNED,
            unique: true,
            allowNull: false
        },
        title: {
            type: DataTypes.STRING,
            allowNull: false
        },
        description: {
            type: DataTypes.TEXT
        }
    }, {
        tableName: 'errors',
        timestamps: false
    });
};