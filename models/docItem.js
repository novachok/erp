var db = require('../sys/db');
//var Doc = db.getModel('doc');

module.exports = function(seq, DataTypes) {

    var DocItem = seq.define("docItem", {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        job: {
            type: DataTypes.STRING,
            allowNull: false
        },
        units: {
            type: DataTypes.INTEGER(3).UNSIGNED,
            defaultValue: 1,
            allowNull: false
        },
        price: {
            type: DataTypes.DECIMAL(10, 2),
            allowNull: false
        }
    }, {
        tableName: 'doc_items',
        timestamps: false
    });

    //DocItem.belongsTo(Doc);

    return DocItem;
};