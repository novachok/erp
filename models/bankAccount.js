var db = require('../sys/db');

module.exports = function(seq, DataTypes) {

    var BankAccount = seq.define("bankAccount", {
        id: {
            type: DataTypes.INTEGER.UNSIGNED,
            primaryKey: true,
            autoIncrement: true
        },
        title: {
            type: DataTypes.STRING,
            allowNull: false
        },
        mfo: {
            type: DataTypes.INTEGER(6).UNSIGNED,
            allowNull: false
        },
        account: {
            type: DataTypes.STRING(20),
            allowNull: false
        },
        currency: {
            type: DataTypes.ENUM('UAH', 'USD', 'EUR'),
            defaultValue: 'UAH'
        },
        isDefault: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        }
    }, {
        tableName: 'bank_accounts',
        timestamps: false,
        getterMethods: {
            currencies: (function(){
                return [
                    'UAH',
                    'USD',
                    'EUR'
                ]
            })()
        }
    });

    return BankAccount;
};