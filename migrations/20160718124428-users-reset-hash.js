'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn(
        'users',
        'resetHash',
        Sequelize.STRING(14)
    )
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.removeColumn('users', 'resetHash');
  }
};
