'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {

        return queryInterface.addColumn(
            'docs',
            'accountId',
            {
                type: Sequelize.INTEGER.UNSIGNED,
                references: {
                    model: 'bank_accounts',
                    key: 'id',
                    onUpdate: 'cascade',
                    onDelete: 'NO ACTION'
                }
            }
        );

    },

    down: function (queryInterface, Sequelize) {

        return queryInterface.removeColumn('docs', 'accountId');

    }
};
