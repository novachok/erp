'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {

        return queryInterface.createTable('docs', {
            id: {
                type: Sequelize.INTEGER.UNSIGNED,
                primaryKey: true,
                autoIncrement: true
            },
            createdAt: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.NOW
            },
            updatedAt: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.NOW
            },
            num: {
                type: Sequelize.INTEGER.UNSIGNED,
                allowNull: false
            },
            isInvoiced: {
                type: Sequelize.BOOLEAN,
                defaultValue: false
            },
            dateOf: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.NOW
            },
            isActed: {
                type: Sequelize.BOOLEAN,
                defaultValue: false
            },
            actOf: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.NOW
            },
            job: {
                type: Sequelize.STRING,
                allowNull: false
            },
            units: {
                type: Sequelize.INTEGER(3).UNSIGNED,
                defaultValue: 1,
                allowNull: false
            },
            price: {
                type: Sequelize.DECIMAL(10, 2),
                allowNull: false
            },
            customerId: {
                type: Sequelize.INTEGER.UNSIGNED,
                references: {
                    model: 'customers',
                    key: 'id',
                    onUpdate: 'cascade',
                    onDelete: 'cascade'
                }
            }
        });

    },

    down: function (queryInterface, Sequelize) {

        return queryInterface.dropTable('docs');

    }
};
