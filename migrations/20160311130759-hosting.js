'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {

    return queryInterface.createTable('hosting', {
      id: {
        type: Sequelize.INTEGER.UNSIGNED,
        primaryKey: true,
        autoIncrement: true
      },
      createdAt: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
      },
      updatedAt: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
      },
      validTo: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
      },
      domain: {
        type: Sequelize.STRING,
        allowNull: false
      },
      account: {
        type: Sequelize.STRING(16),
        allowNull: false
      },
      onlyHosting: {
        type: Sequelize.BOOLEAN,
        defaultValue: 0
      },
      onlyDomain: {
        type: Sequelize.BOOLEAN,
        defaultValue: 0
      },
      space: {
        type: Sequelize.STRING
      },
      notes: {
        type: Sequelize.TEXT
      },
      customerId: {
        type: Sequelize.INTEGER.UNSIGNED,
        references: {
          model: 'customers',
          key: 'id',
          onUpdate: 'cascade',
          onDelete: 'cascade'
        }
      }
    });

  },

  down: function (queryInterface, Sequelize) {

    return queryInterface.dropTable('hosting');

  }
};
