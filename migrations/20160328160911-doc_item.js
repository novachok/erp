'use strict';

module.exports = {

    up: function (queryInterface, Sequelize) {

        return queryInterface.removeColumn('docs', 'job')
            .then(function () {
                return queryInterface.removeColumn('docs', 'units');
            })
            .then(function () {
                return queryInterface.removeColumn('docs', 'price');
            })
            .then(function() {
                return queryInterface.createTable('doc_items', {
                    id: {
                        type: Sequelize.INTEGER.UNSIGNED,
                        primaryKey: true,
                        autoIncrement: true
                    },
                    job: {
                        type: Sequelize.STRING,
                        allowNull: false
                    },
                    units: {
                        type: Sequelize.INTEGER(3).UNSIGNED,
                        defaultValue: 1,
                        allowNull: false
                    },
                    price: {
                        type: Sequelize.DECIMAL(10, 2),
                        allowNull: false
                    },
                    docId: {
                        type: Sequelize.INTEGER.UNSIGNED,
                        references: {
                            model: 'docs',
                            key: 'id',
                            onUpdate: 'cascade',
                            onDelete: 'cascade'
                        }
                    }
                });
            });

    },

    down: function (queryInterface, Sequelize) {

        return queryInterface.addColumn(
                'docs',
                'job',
                {
                    type: Sequelize.STRING,
                    allowNull: false
                }
            )
            .then(function () {
                return queryInterface.addColumn(
                    'docs',
                    'units',
                    {
                        type: Sequelize.INTEGER(3).UNSIGNED,
                        defaultValue: 1,
                        allowNull: false
                    }
                );
            })
            .then(function () {
                return queryInterface.addColumn(
                    'docs',
                    'price',
                    {
                        type: Sequelize.DECIMAL(10, 2),
                        allowNull: false
                    }
                );
            })
            .then(function() {
                return queryInterface.dropTable('doc_items');
            });

    }
};
