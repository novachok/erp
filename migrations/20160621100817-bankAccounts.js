'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        return queryInterface.removeColumn('users', 'bank')
            .then(function () {
                return queryInterface.removeColumn('users', 'mfo');
            })
            .then(function () {
                return queryInterface.removeColumn('users', 'account');
            })
            .then(function() {
                return queryInterface.createTable('bank_accounts', {
                    id: {
                        type: Sequelize.INTEGER.UNSIGNED,
                        primaryKey: true,
                        autoIncrement: true
                    },
                    title: {
                        type: Sequelize.STRING,
                        allowNull: false
                    },
                    mfo: {
                        type: Sequelize.INTEGER(6).UNSIGNED,
                        allowNull: false
                    },
                    account: {
                        type: Sequelize.STRING(20),
                        allowNull: false
                    },
                    currency: {
                        type: Sequelize.ENUM('UAH', 'USD', 'EUR'),
                        defaultValue: 'UAH'
                    },
                    isDefault: {
                        type: Sequelize.BOOLEAN,
                        defaultValue: false
                    },
                    userId: {
                        type: Sequelize.INTEGER.UNSIGNED,
                        references: {
                            model: 'users',
                            key: 'id',
                            onUpdate: 'cascade',
                            onDelete: 'cascade'
                        }
                    }
                });
            });
    },

    down: function (queryInterface, Sequelize) {
        return queryInterface.addColumn('users', 'bank', Sequelize.STRING)
            .then(function() {
                return queryInterface.addColumn('users', 'mfo', Sequelize.STRING(10));
            })
            .then(function() {
                return queryInterface.addColumn('users', 'account', Sequelize.STRING(20)
                );
            })
            .then(function() {
                return queryInterface.dropTable('bank_accounts');
            });
    }
};
