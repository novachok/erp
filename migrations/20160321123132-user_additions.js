'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {

        return queryInterface.addColumn(
                'users',
                'title',
                Sequelize.STRING
            )
            .then(function(){
                return queryInterface.addColumn(
                    'users',
                    'inn',
                    Sequelize.STRING(10)
                );
            })
            .then(function(){
                return queryInterface.addColumn(
                    'users',
                    'phone',
                    Sequelize.STRING(20)
                );
            })
            .then(function(){
                return queryInterface.addColumn(
                    'users',
                    'address',
                    Sequelize.STRING
                );
            })
            .then(function(){
                return queryInterface.addColumn(
                    'users',
                    'bank',
                    Sequelize.STRING
                );
            })
            .then(function(){
                return queryInterface.addColumn(
                    'users',
                    'mfo',
                    Sequelize.STRING(10)
                );
            })
            .then(function(){
                return queryInterface.addColumn(
                    'users',
                    'account',
                    Sequelize.STRING(20)
                );
            });
    },

    down: function (queryInterface, Sequelize) {

        return queryInterface.removeColumn('users', 'title')
            .then(function(){
                return queryInterface.removeColumn('users', 'inn');
            })
            .then(function(){
                return queryInterface.removeColumn('users', 'phone');
            })
            .then(function(){
                return queryInterface.removeColumn('users', 'address');
            })
            .then(function(){
                return queryInterface.removeColumn('users', 'bank');
            })
            .then(function(){
                return queryInterface.removeColumn('users', 'mfo');
            })
            .then(function(){
                return queryInterface.removeColumn('users', 'account');
            });

    }
};
