'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn(
          'hosting',
          'isOff',
          {
            type: Sequelize.BOOLEAN,
            defaultValue: 0
          }
      );
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.removeColumn('hosting', 'isOff');
  }
};
