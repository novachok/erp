'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('errors', {
      id: {
        type: Sequelize.INTEGER.UNSIGNED,
        primaryKey: true,
        autoIncrement: true
      },
      code: {
        type: Sequelize.INTEGER.UNSIGNED,
        allowNull: false,
        unique: true
      },
      status: {
        type: Sequelize.INTEGER.UNSIGNED
      },
      title: {
        type: Sequelize.STRING,
        allowNull: false
      },
      description: {
        type: Sequelize.TEXT
      }
    });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('errors');
  }
};
