var rewire = require('rewire');
var expect = require('chai').expect;
var C      = require('../public/jsx/flux/constants');


describe('React:stores', () => {

    var stores = {};

    beforeEach(() => {
        stores.IdentityStore = rewire('../public/jsx/flux/store/identity');
        stores.IdentityCallback = stores.IdentityStore.__get__('storeCallback');
    });

    describe('Identity', () => {

        it('get identity', () => {

            stores.IdentityCallback({
                source: 'VIEW_ACTION',
                action: {
                    actionType: C.IDENTITY_SET,
                    data: {
                        token: 111,
                        user: {
                            id: 1,
                            email: 'test@mail.com'
                        }
                    }
                }
            });

            expect(stores.IdentityStore.getToken()).equal(111);
            expect(stores.IdentityStore.getUser()).deep.equal({id: 1, email: 'test@mail.com'});

        });

    });

});
