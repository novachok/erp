require('./testdom')('<html><body><script src="http://code.jquery.com/jquery-2.1.1.js"></script></body></html>', {
    done: function(err, window) {
        console.log('window done');
    }
});

var expect    = require('chai').expect;
var React     = require('react');
var TestUtils = require('react-addons-test-utils');
var rewire    = require('rewire');

describe('React:components', () => {

    it('test login form', () => {
        var LoginForm    = require('../public/jsx/components/formLogin.jsx');
        var testLogin    = null;
        var testPass     = null;
        var testRemember = false;

        var authFunc = function(login, pass, remember){
            testLogin = login;
            testPass = pass;
            testRemember = remember;
        };
        var el = TestUtils.renderIntoDocument(
            <LoginForm error={"test error"} authorize={authFunc} />
        );

        var alertEl = TestUtils.findRenderedDOMComponentWithClass(el, 'alert');
        expect(alertEl.textContent).equal('test error');

        var inputs = TestUtils.scryRenderedDOMComponentsWithTag(el, 'input');
        inputs[0].value = 'test Login';
        TestUtils.Simulate.change(inputs[0]);
        inputs[1].value = 'test pass';
        TestUtils.Simulate.change(inputs[1]);
        inputs[2].checked = true;
        TestUtils.Simulate.change(inputs[2]);

        var form = TestUtils.findRenderedDOMComponentWithTag(el, 'form');
        TestUtils.Simulate.submit(form);

        expect(testLogin).equal('test Login');
        expect(testPass).equal('test pass');
        expect(testRemember).equal(true);
    });

    it('test nl2br element', () => {
        var Nl2Br = require('../public/jsx/components/nl2br.jsx');
        var text = 'hello \n world \n test br';
        var el = TestUtils.renderIntoDocument(
            <Nl2Br text={text} />
        );
        var brs = TestUtils.scryRenderedDOMComponentsWithTag(el, 'br');

        expect(brs.length).equal(2);
    });
});