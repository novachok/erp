var expect = require('chai').expect;
var _    = require('../lib/_');

describe('Underscore extend', function(){

    it('#capitalize() - Capitalization of the string', function(){
        var result = _.capitalize('hello world');
        expect(result).equal('Hello world');
    });

    it('#unique() - Generation of unique string', function(){
        var u1 = _.unique(7);
        var u2 = _.unique(7);
        expect(u1).is.a('string');
        expect(u1).has.length(7);
        expect(u1).not.equal(u2);
    });

    it("#isNumeric() - Testing if the string is numeric", function(){
        expect(_.isNumeric('555')).equal(true);
        expect(_.isNumeric(44)).equal(true);
        expect(_.isNumeric('44a')).equal(false);
        expect(_.isNumeric('d111')).equal(false);
        expect(_.isNumeric('aaa')).equal(false);
        expect(_.isNumeric(true)).equal(false);
    });

    it('#deepExtend() - Deep merge of objects', function(){
        var value = {
            foo: 1,
            baz: {
                bar: 1,
                foo: 2
            }
        };
        var ext = {
            bar: 3,
            baz: {
                foo: [3,2],
                more: {
                    foo: 1
                }
            }
        };
        var result = _.deepExtend(value, ext);
        expect(result).deep.equal({
            foo: 1,
            bar: 3,
            baz: {
                bar: 1,
                foo: [3,2],
                more: {
                    foo: 1
                }
            }
        });
    });

});