module.exports = function(markup, options) {
    if (typeof document !== 'undefined') return;

    var jsdom = require('jsdom');

    options = {
        virtualConsole: jsdom.createVirtualConsole().sendTo(console),
        done: function() {
            console.log('window done!');
        }
    };

    global.document = jsdom.jsdom((markup || ''), options);
    global.window = global.document.defaultView;
    global.navigator = {
        userAgent: 'node.js'
     };

    jsdom.jQueryify(global.window, "http://code.jquery.com/jquery-2.1.1.js", function () {
        global.window.$("body").append('<div class="testing">Hello World, It works</div>');
        console.log(global.window.$(".testing").text());
    });
};