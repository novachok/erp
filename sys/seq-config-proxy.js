var config = require("./config");

var _tmp = {
    "username": config.db.username,
    "password": config.db.password,
    "database": config.db.database,
    "host": config.db.host,
    "dialect": "mysql",
    "migrationPath": "db/migration"
};
var _c = {};
_c[config.environment] = _tmp;

module.exports = _c;