var fs      = require('fs');
var path    = require('path');
var winston = require('winston');
var mailer  = require('nodemailer');
var _       = require('../lib/_');

var config = JSON.parse(fs.readFileSync(path.resolve(process.cwd() +'/config.json')));
var _conf = null;

module.exports = (function(){
    if(_conf)
    {
        return _conf;
    }

    var env = 'NODE_ENV' in process.env ? process.env.NODE_ENV : 'default';

    _conf = env in config ? config[env] : config.default;
    if(env != 'default')
    {
        var _tmp = {};
        _tmp = _.extend(_tmp, config.default);
        _.deepExtend(_tmp, _conf);
        _conf = _tmp;
    }
    _conf.environment = env;

    // Setup mailer
    var mail_dsn = _conf.mail.transport + "://" + _conf.mail.username +":"+ _conf.mail.password + "@"+ _conf.mail.host;
    _conf.mailer = mailer.createTransport(mail_dsn);

    // Start logger
    if(_conf.logger.enabled)
    {
        var transports = [];
        for(var i=0; i<_conf.logger.transports.length; i++)
        {
            var transport = _conf.logger.transports[i];
            if('filename' in transport)
            {
                transport.filename = process.cwd() + transport.filename;
            }
            transports.push(new (winston.transports[_.capitalize(transport.type)])(transport));
        }

        _conf.logger = new (winston.Logger)({
            level: _conf.logger.level,
            transports: transports
        });
        _conf.logger.exitOnError = false;

    } else {
        // Log placeholder
        _conf.logger = {
            log: function(){}
        };
    }

    return _conf;
})();
