var crypto  = require('crypto');
var promise = require('promise');
var config  = require('./config');
var db      = require('./db');
var _       = require('../lib/_');

var _identity = null;

function _setIdentity(tk, user) {
    _identity = {
        token: tk && tk.token ? tk.token : null,
        user: user.safeData
    };

    return _identity;
}

var security = {
    identity: null,
    sha256: function(string, salt) {
        var hash = crypto.createHash('sha256');
        hash.update(salt + string + config.security.salt);

        return hash.digest('hex');
    },
    authorize: function(login, password) {
        var self = this;
        var p = new promise(function(resolve, reject){

            var User = db.getModel('user');
            User.findOne({
                    where: {
                        email: login
                    }
                })
                .then(function(user){
                    if(user && user.password == self.sha256(password, user.salt))
                    {
                        var Token = db.getModel('token');
                        return Token.findOrCreate({
                                where: {userId: user.id},
                                defaults: {
                                    token: _.unique(32)
                                }
                            })
                            .spread(function(token, created){
                                if(created)
                                {
                                    token
                                        .setUser(user)
                                        .then(function(){
                                            return resolve(_setIdentity(token, user));
                                        })
                                        .catch(function(err){
                                            return reject(err);
                                        });
                                } else {
                                    // TODO: update timestamp of last usage
                                    token.updatedAt = new Date();
                                    token
                                        .save()
                                        .then(function(){
                                            return resolve(_setIdentity(token, user));
                                        })
                                        .catch(function(err){
                                            return reject(err);
                                        });
                                }
                            });

                    } else {
                        var err = new Error(1001);
                        return reject(err);
                    }
                })
                .catch(function(err) {
                    reject(err);
                });
        });
        return p;
    },
    reset: function(login) {
        var p = new promise(function(resolve, reject) {

            var User = db.getModel('user');
            User.findOne({
                    where: {
                        email: login
                    }
                })
                .then(function(user) {
                    if(user) {

                        user.resetHash = _.unique(14);
                        return user.save()
                            .then(function(){
                                var mailOptions = {
                                    from: '"Artinet ERP" <office@artinet.com.ua>',
                                    to: user.email,
                                    subject: 'Reset password to ERP',
                                    text: 'Reset link: ' + config.http.protocol +'://'+ config.http.address + ':'+ config.http.port +'/reset/'+ user.resetHash
                                };

                                return config.mailer.sendMail(mailOptions, function(err, info){
                                    if(err){
                                        reject(err);
                                    } else {
                                        resolve();
                                    }
                                });
                            })
                            .catch(function(err) {
                                return reject(err);
                            });
                    } else {
                        var err = new Error(1001);
                        return reject(err);
                    }
                })
                .catch(function(err) {
                    return reject(err);
                });
        });
        return p;
    },
    getIdentity: function() {
        return _identity;
    }
};

module.exports = security;