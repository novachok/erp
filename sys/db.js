var path   = require('path');
var Seq    = require('sequelize');
var config = require('./config');

var db = {
    _models: {},
    getModel: function(name) {
        var self = this;
        if(!(name in self._models))
        {
            this._models[name] = this.connection.import(path.normalize(__dirname +'/../models/'+ name));
        }
        return this._models[name];
    },
    count: function(column, alias) {
        var seq = this.connection.queryInterface.sequelize;
        return [
            seq.fn('COUNT', seq.col(column)),
            (alias || column)
        ];
    }
};
db.connection = new Seq(config.db.database, config.db.username, config.db.password, {
    host: config.db.host,
    dialect: 'mysql'
});

module.exports = db;