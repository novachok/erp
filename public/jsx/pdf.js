var PDFDocument   = require('pdfkit');
var BlobStream    = require('blob-stream');
var fs            = require('fs');
var IdentityStore = require('./flux/store/identity');

var _doc, _pdf, fonts = {};

loadFont('/fonts/opensans/OpenSans-Regular.ttf', 'OpenSans', fonts);
loadFont('/fonts/opensans/OpenSans-Bold.ttf', 'OpenSansBold', fonts);
loadFont('/fonts/opensans/OpenSans-Italic.ttf', 'OpenSansItalic', fonts);


function loadFont(path, name, fonts) {
    var oReq = new XMLHttpRequest();
    oReq.open("GET", path, true);
    oReq.responseType = "arraybuffer";
    oReq.onload = function(oEvent) {
        var arrayBuffer = oReq.response;
        if (arrayBuffer) {
            fonts[name] = arrayBuffer;
        }
    };
    oReq.send(null);
}

function _initPdf() {
    var user = IdentityStore.getUser();

    _pdf = new PDFDocument();
    for(var fontName in fonts) {
        _pdf.registerFont(fontName, fonts[fontName]);
    }

    console.log(_doc);
    _pdf.fontSize(9)
        .font('OpenSansBold')
        .text('Постачальник: ', 270, 20, {continued: true})
        .font('OpenSans')
        .text(user.title)
        .moveDown(0.7)
        .text('ЄДРПОУ: '+ user.inn +', тел.:'+ user.phone)
        .text('р/р: '+ _doc.account.account +' у '+ _doc.account.title +', МФО: '+ _doc.account.mfo)
        .text('Є платником єдиного податку')
        .text('Адреса: '+ user.address);

    _pdf.moveTo(40, 98)
        .lineTo(580, 98)
        .lineWidth(0.5)
        .stroke("#777777");
}

function _drowTable(y) {
    // Drow table
    _invoiceRow(y, ['№', 'Назва', 'Од', 'Кількість', 'Ціна без ПДВ', 'Сума без ПДВ'], true, true);

    var total = 0;
    var i = 1;
    y = y + 17;

    for(var x in _doc.items) {
        var item = _doc.items[x];
        total += (item.units * item.price);
        _invoiceRow(y, [i, item.job, 'шт.', item.units, item.price.toFixed(2), (item.units * item.price).toFixed(2)]);
        i++;
        y = y + 17;
    }
    _invoiceRow(y, [null, null, null, null, 'Всього:', total.toFixed(2)], true);

    return {y: y, total: total};
}

function _invoiceRow(y, val, isBold, isFill) {
    var rowHeight   = 17;
    var shiftLeft   = 8;
    var shiftTop    = 2;
    var strokeColor = "#777777";
    var textColor   = '#000000';
    var cols = [40, 70, 325, 361, 419, 500]

    isBold = typeof(isBold) != 'undefined' ? true : false;
    isFill = typeof(isFill) != 'undefined' ? true : false;

    _pdf.fontSize(8)
        .rect(40, y, 540, rowHeight)
        .lineWidth(0.4);
    if(isFill) {
        _pdf.fillAndStroke("#EEEEEE", strokeColor);
    } else {
        _pdf.stroke(strokeColor);
    }

    if(isBold) {
        _pdf.font('OpenSansBold');
    } else {
        _pdf.font('OpenSans')
    }

    if(val[0]) {
        _pdf.moveTo(cols[1], y)
            .lineTo(cols[1], (y + rowHeight))
            .stroke(strokeColor)
            .fillColor(textColor)
            .text(val[0], (cols[0] + shiftLeft), (y + shiftTop));
    }

    if(val[1]) {
        _pdf.moveTo(cols[2], y)
            .lineTo(cols[2], (y + rowHeight))
            .stroke(strokeColor)
            .fillColor(textColor)
            .text(val[1], (cols[1] + shiftLeft), (y + shiftTop));
    }

    if(val[2]) {
        _pdf.moveTo(cols[3], y)
            .lineTo(cols[3], (y + rowHeight))
            .stroke(strokeColor)
            .fillColor(textColor)
            .text(val[2], (cols[2] + shiftLeft), (y + shiftTop));
    }

    if(val[3]) {
        _pdf.moveTo(cols[4], y)
            .lineTo(cols[4], (y + rowHeight))
            .stroke(strokeColor)
            .fillColor(textColor)
            .text(val[3], (cols[3] + shiftLeft), (y + shiftTop));
    }

    if(val[4]) {
        _pdf.moveTo(cols[5], y)
            .lineTo(cols[5], (y + rowHeight))
            .stroke(strokeColor)
            .fillColor(textColor)
            .text(val[4], (cols[4] + shiftLeft), (y + shiftTop));
    }

    if(val[5]) {
        _pdf.text(val[5], (cols[5] + shiftLeft), (y + shiftTop), {width: 60});
    }
}

function _num2string(num) {

    var str = '';
    num = num.toFixed(2);
    var f = num.substring(num.indexOf('.')+1);
    var d = num.substring(0, num.indexOf('.'));

    var words = {
        '0': 'нуль',
        '1': 'одна',
        '2': 'дві',
        '3': 'три',
        '4': 'чотири',
        '5': 'п\'ять',
        '6': 'шість',
        '7': 'сімь',
        '8': 'вісім',
        '9': 'дев\'ять',
        '10': 'десять',
        '11': 'одинадцять',
        '12': 'дванадцять',
        '13': 'тринадцять',
        '14': 'чотирнадцять',
        '15': 'п\'ятнадцять',
        '16': 'шістнадцять',
        '17': 'сімнадцять',
        '18': 'вісімнадцать',
        '19': 'дев\'ятнадцять',
        '20': 'двадцять',
        '30': 'тридцять',
        '40': 'сорок',
        '50': 'пятдесят',
        '60': 'шістдесят',
        '70': 'сімдесят',
        '80': 'вісімдесят',
        '90': 'дев\'яносто',
        '100': 'сто',
        '200': 'двісті',
        '300': 'триста',
        '400': 'чотириста',
        '500': 'п\'ятсот',
        '600': 'шістсот',
        '700': 'сімсот',
        '800': 'вісімсот',
        '900': 'дев\'ятсот',
        '1k': 'тисяча',
        '1ki': 'тисячі',
        '1km': 'тисяч'
    };

    d = d * 1;
    while(d > 0) {
        if(d > 99000) {
            d = 0;
        }

        if(d > 4999) {
            var k = Math.round((d / 1000)).toString();
            if(typeof(words[k]) == 'undefined') {
                str += words[k.substring(0, 1)+'0'] + ' ' + words[k.substring(1)];
            } else {
                str += words[k];
            }
            str += ' ' + words['1km'] + ' ';
            d = d % 1000;
        } else if(d > 1999) {
            var k = d.toString().substring(0, 1);
            str += words[k] + ' ' + words['1ki'] + ' ';
            d = d % 1000;
        } else if(d > 999) {
            str += words['1'] + ' ' + words['1k'] + ' ';
            d = d % 1000;
        } else if(d > 99) {
            var k = d.toString().substring(0, 1) + '00';
            str += words[k] + ' ';
            d = d % 100;
        } else if(d < 20) {
            str += words[d.toString()];
            d = 0;
        } else if(d > 19) {
            var k = d.toString().substring(0, 1) + '0';
            str += words[k] + ' ';
            d = d % 10;
        }
    }

    return str +' грн, '+ f + ' коп.';
}

function _sendPdf() {
    if(_pdf) {
        _pdf.end();
        var stream = _pdf.pipe(BlobStream());
        stream.on('finish', function() {
            var url = stream.toBlobURL('application/pdf');
            window.open(url);
        });
    }
    return null;
}


module.exports = {

    getAct: function(doc) {
        _doc = doc;
        var user = IdentityStore.getUser();

        _initPdf();

        _pdf.moveDown(2)
            .font('OpenSansBold')
            .fontSize(12)
            .text('Акт №-'+ _doc.num, 40, 120, {align: 'center'})
            .font('OpenSans')
            .fontSize(11)
            .text('здачі прийняття робіт (наданя послуг)', {align: 'center'});

        _pdf.moveDown(2)
            .font('OpenSans')
            .fontSize(9)
            .text('Ми, представники Замовника ', {continued: true})
            .font('OpenSansBold')
            .text(_doc.customer.title, {continued: true})
            .font('OpenSans')
            .text(', з одного боку, та представник виконавця ', {continued: true})
            .font('OpenSansBold')
            .text(user.title, {continued: true})
            .font('OpenSans')
            .text(', з іншого боку, склали цей акт про те що Виконавцем були проведені такі роботи (надані такі послуги):');

        var p = _drowTable(220);

        _pdf.font('OpenSans')
            .fontSize(9)
            .text('Загальна вартість робіт (послуг) без ПДВ: ', 40, (p.y + 30), {continued: true})
            .font('OpenSansBold')
            .text(_num2string(p.total), {continued: true})
            .font('OpenSans')
            .text(', ПДВ: 0 грн, 00 коп. (не нараховується).')
            .text('Сторони претензій одна до одної не мають.');

        _pdf.moveDown(2)
            .font('OpenSansBold')
            .text('Від виконавця:', {continued: true})
            .text('Від замовника:', 250);

        var y = Math.round(_pdf.y + 27);
        _pdf.moveTo(40, y)
            .lineTo(200, y)
            .stroke("#000000")
            .moveTo(321, y)
            .lineTo(481, y)
            .stroke("#000000");

        var actOf = dateFormat('%d.%m.%Y', new Date(_doc.actOf));
        _pdf.moveDown()
            .font('OpenSans')
            .text(actOf, 40, (_pdf.y+15), {continued: true})
            .text(actOf, 275);

        _pdf.text(user.title, {continued: true})
            .text(_doc.customer.title, 240);

        _sendPdf();
    },

    getInvoice: function(doc) {
        _doc = doc;
        _initPdf();

        _pdf.text('Отримувач: ', 40, 120, {continued: true})
            .font('OpenSansBold')
            .text(_doc.customer.title)
            .font('OpenSans');

        var dateOf = new Date(_doc.dateOf);

        _pdf.moveDown(2)
            .font('OpenSansBold')
            .fontSize(12)
            .text('Рахунок-Фактура №-'+ _doc.num, {align: 'center'})
            .font('OpenSans')
            .fontSize(11)
            .text('від: '+ dateFormat('%d.%m.%Y', dateOf), {align: 'center'});

        var p = _drowTable(200);

        _pdf.fontSize(9)
            .font('OpenSans')
            .text('Всього на суму: ', 40, (p.y + 40), {continued: true})
            .font('OpenSansItalic')
            .text(_num2string(p.total));

        _pdf.font('OpenSans')
            .text('Підписав(ла): ', 300, (p.y + 80))
            .moveTo(360, (p.y + 92))
            .lineTo(540, (p.y + 92))
            .stroke("#000000");

        _sendPdf();
    }
};