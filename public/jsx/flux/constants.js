var constants = {

    NOTIFICATION_ADD: 1,
    NOTIFICATION_LOCK: 2,
    NOTIFICATION_UNLOCK: 3,

    IDENTITY_LOAD: 4,
    IDENTITY_SET: 5,
    IDENTITY_UPDATE: 6,
    IDENTITY_UNSET: 7,

    CUSTOMER_LOAD: 8,
    CUSTOMER_CREATE: 9,
    CUSTOMER_UPDATE: 10,
    CUSTOMER_DELETE: 11,

    HOSTING_LOAD: 12,
    HOSTING_CREATE: 13,
    HOSTING_UPDATE: 14,
    HOSTING_DELETE: 15,

    DOC_LOAD: 16,
    DOC_CREATE: 17,
    DOC_UPDATE: 18,
    DOC_DELETE: 19,
    DOC_CHANGE_ITEMS: 20,
    DOC_PHONY: 21,

    STR_DOC_ITEM_HOSTING: 'надання послуг з розміщення веб-сайту (хостинг)'
};

module.exports = constants;