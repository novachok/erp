import { browserHistory } from 'react-router';

var assign             = require('object-assign');
var EventEmitter       = require('events').EventEmitter;
var Dispatcher         = require('../dispatcher');
var C                  = require('../constants');
var _                  = require('../../../../lib/_');
var connectClass       = require('../../connect');

var connect = new connectClass();

var CHANGE_EVENT = 'change';

const docCallback = (payload) => {
    var action = payload.action;

    switch(action.actionType) {

        case C.DOC_CREATE:
            createDoc(action.data);
            break;

        case C.DOC_CHANGE_ITEMS:
            var i = action.num * 1;
            if(i != _items) {
                _items = i;
                _docs = {};
                browserHistory.push('/docs');
                Doc.emitChange();
            }
            break;

        case C.DOC_PHONY:
            _docsById['-1'] = action.data;
            break;

        case C.DOC_UPDATE:
            updateDoc(action.id, action.data);
            break;
    }

    return true;
};

Dispatcher.register(docCallback);

var _items = 15;
var _total = 0;
var _docs  = {};
var _docsById = {};

function loadDocs(page, lock) {
    var data = {
        start: page * _items - _items,
        items: _items
    };

    var options = {
        success: function(response) {
            if(response.data.total > 0) {
                _total = response.data.total;
                _docs[('p_'+page)] = response.data.docs;
                for(var i=0; i<response.data.docs.length; i++) {
                    _docsById[response.data.docs[i].id] = response.data.docs[i];
                }
                if(lock == true) {
                    Doc.emitChange();
                }
            }
        }
    };
    if(lock) {
        options.lock = 'Loading data, please wait ...';
    }
    connect.get('/api/docs', data, options);
}


function createDoc(data) {
    connect.post('/api/doc', data, {
        lock: 'Saving data, please wait ...',
        success: function(response) {
            _docsById[response.data.id] = response.data;
            var path = '/docs/edit/'+ response.data.id;
            browserHistory.push(path);
            // TODO: reload page of docs
        }
    });
}

function updateDoc(id, data) {
    connect.put('/api/doc/'+ id, data, {
        lock: 'Saving data, please wait ...',
        success: function(response) {
            _docsById[response.data.id] = response.data;
            Doc.emitChange();
        }
    });
}

var Doc = assign({}, EventEmitter.prototype, {

    emitChange: function() {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener: function(callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    },

    getAll: function(page) {
        page = page || 1;
        var _d = [];

        var k = 'p_'+ page;
        if(k in _docs) {
            _d = _docs[k];
        } else {
            loadDocs(page, true);
        }

        return _d;
    },

    get: function(id) {
        if(_.isNumeric(id)) {
            if(id in _docsById) {
                return _docsById[id];
            }
        }

        return null;
    },

    getOrEmpty: function(id) {
        var doc = this.get(id);
        return doc || {
                id: null,
                isInvoiced: false,
                dateOf: null,
                isActed: false,
                actOf: null,
                num: null,
                items: [],
                customerId: "",
                customer: {}
            };
    },

    itemsOnPage: function() {
        return _items;
    },

    itemsTotal: function() {
        return _total;
    }
});

module.exports = Doc;