var EventEmitter = require('events').EventEmitter;
var assign       = require('object-assign');
var Dispatcher   = require('../dispatcher');
var C            = require('../constants');
var _            = require('../../../../lib/_');
var connectClass = require('../../connect');

var connect = new connectClass();
var CHANGE_EVENT = 'change';

const storeCallback = (payload) => {
    var action = payload.action;

    switch(action.actionType) {

        case C.IDENTITY_LOAD:
            var token = action.data;
            loadByToken(token);
            break;

        case C.IDENTITY_SET:
            var data = action.data;
            setIdentity(data);
            break;

        case C.IDENTITY_UPDATE:
            var data   = action.data;
            updateIdentity(data);
            break;

        case C.IDENTITY_UNSET:
            _identity = {token: false, user: null};
            _.delCookie('token');
            Identity.emitChange();
            break;
    }

    return true;
};
Dispatcher.register(storeCallback);

/**
 *
 * @private
 */
var _identity = {};


function setIdentity(data) {
    _identity = assign(_identity, data);
    Identity.emitChange();
}

function updateIdentity(data) {

    connect.put('/api/identity', data, {
        lock: 'Saving data, please wait ...',
        success: function(response) {
            setIdentity({user: response.data});
        }
    });
}

function loadByToken(token) {

    connect.get('/api/identity', {}, {
        lock: 'Loading data, please wait ...',
        success: function(response) {
            setIdentity(response.data);
        }
    });
}


var Identity = assign({}, EventEmitter.prototype, {

    get: function() {
        return _identity;
    },

    getToken: function() {
        return _identity.token || null;
    },

    getUser: function() {
        return _identity.user || null;
    },

    getAccounts: function() {
        return _identity.user ? _identity.user.accounts : [];
    },

    emitChange: function() {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener: function(callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

module.exports = Identity;