import { browserHistory } from 'react-router'

var EventEmitter       = require('events').EventEmitter;
var assign             = require('object-assign');
var Dispatcher         = require('../dispatcher');
var C                  = require('../constants');
var _                  = require('../../../../lib/_');
var connectClass       = require('../../connect');

var connect = new connectClass();
var CHANGE_EVENT = 'change';

const storeCallback = (payload) => {
    var action = payload.action;

    switch(action.actionType) {

        case C.CUSTOMER_LOAD:
            loadCustomers();
            break;

        case C.CUSTOMER_CREATE:
            createCustomer(action.data);
            break;

        case C.CUSTOMER_UPDATE:
            updateCustomer(action.id, action.data);
            break;

        case C.CUSTOMER_DELETE:
            deleteCustomer(action.id);
            break;
    }

    return true;
};
Dispatcher.register(storeCallback);

var _customers = [];
var _initLoad = false;


/**
 * Load array of customers
 */
function loadCustomers() {
    connect.get('/api/customers', {}, {
        lock: 'Loading data, please wait ...',
        success: function(response) {
            _customers = response.data;
            Customers.emitChange();
        }
    });
};

/**
 * Create a customer
 *
 * @param data
 */
function createCustomer(data) {
    connect.post('/api/customer', data, {
        lock: 'Saving data, please wait ...',
        success: function(response) {
            _customers.push(response.data);
            var path = '/customer/edit/'+ response.data.id;
            browserHistory.push(path);
        }
    });
};

/**
 * Update the customer
 *
 * @param id
 * @param data
 */
function updateCustomer(id, data) {
    connect.put('/api/customer/'+id, data, {
        lock: 'Saving data, please wait ...',
        success: function(response) {
            for(var i=0; i<_customers.length; i++) {
                if(_customers[i].id == id) {
                    _customers[i] = response.data;
                    break;
                }
            }
            Customers.emitChange();
        }
    });
};

function deleteCustomer(id) {
    connect.rm('/api/customer/'+id, {}, {
        success: function(response) {
            for(var i=0; i<_customers.length; i++) {
                if(_customers[i].id == id) {
                    _customers.splice(i, 1);
                }
            }
            Customers.emitChange();
            const path = '/customers';
            browserHistory.push(path);
        }
    });
}

var Customers = assign({}, EventEmitter.prototype, {

    get: function(id) {
        if(_.isNumeric(id)) {
            for (var i = 0; i < _customers.length; i++) {
                if (_customers[i].id == id) {
                    return _customers[i];
                }
            }
        }

        return null;
    },

    getOrEmpty: function(id) {
        var customer = this.get(id);
        return customer || {
                id: null,
                title: null,
                contact: null,
                email: null,
                phone: null,
                notes: null
            };
    },

    getAll: function() {
        if(!_initLoad) {
            _initLoad = true;
            loadCustomers();
        }

        return _customers;
    },

    emitChange: function() {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener: function(callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

module.exports = Customers;