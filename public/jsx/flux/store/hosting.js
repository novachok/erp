import { browserHistory } from 'react-router';

var EventEmitter       = require('events').EventEmitter;
var assign             = require('object-assign');
var Dispatcher         = require('../dispatcher');
var C                  = require('../constants');
var _                  = require('../../../../lib/_');
var connectClass       = require('../../connect');

var connect = new connectClass();
var CHANGE_EVENT = 'change';

const storeCallback = (payload) => {
    var action = payload.action;

    switch(action.actionType) {

        case C.HOSTING_LOAD:
            loadHosts();
            break;

        case C.HOSTING_CREATE:
            createHost(action.data);
            break;

        case C.HOSTING_UPDATE:
            updateHost(action.id, action.data);
            break;

        case C.HOSTING_DELETE:
            deleteHost(action.id);
            break;
    }

    return true;
};
Dispatcher.register(storeCallback);

var _hosts = [];
var _initLoad = false;

function createHost(data) {
    connect.post('/api/hosting', data, {
        lock: 'Saving data, please wait ...',
        success: function(response) {
            _hosts.unshift(response.data);
            var path = '/hosting/edit/'+ response.data.id;
            browserHistory.push(path);
        }
    });
}

function updateHost(id, data) {
    connect.put('/api/hosting/'+ id, data, {
        lock: 'Saving data, please wait ...',
        success: function(response) {
            for(var i=0; i<_hosts.length; i++) {
                if(_hosts[i].id == id) {
                    _hosts[i] = response.data;
                    break;
                }
            }
            Hosting.emitChange();
        }
    });
}

function loadHosts() {
    connect.get('/api/hosting', {}, {
        lock: 'Loading data, please wait ...',
        success: function(response) {
            _hosts = response.data;
            Hosting.emitChange();
        }
    });
}

function deleteHost(id) {
    connect.rm('/api/hosting/'+ id, {}, {
        success: function() {
            for(var i=0; i<_hosts.length; i++) {
                if(_hosts[i].id == id) {
                    _hosts.splice(i, 1);
                }
            }
            Hosting.emitChange();
            const path = '/hosting';
            browserHistory.push(path);
        }
    });
}


var Hosting = assign({}, EventEmitter.prototype, {

    get: function(id) {
        if(_.isNumeric(id)) {
            for (var i = 0; i < _hosts.length; i++) {
                if (_hosts[i].id == id) {
                    return _hosts[i];
                }
            }
        }

        return null;
    },

    getOrEmpty: function(id) {
        var host = this.get(id);
        return host || {
                id: null,
                account: null,
                domain: null,
                space: null,
                validTo: null,
                onlyHosting: false,
                onlyDomain: false,
                isOff: false,
                notes: null,
                customerId: null,
                customer: {}
            };
    },

    getAll: function() {
        if(!_initLoad) {
            loadHosts();
            _initLoad = true;
        }

        return _hosts;
    },

    emitChange: function() {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener: function(callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }

});

module.exports = Hosting;