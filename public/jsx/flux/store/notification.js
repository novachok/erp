var EventEmitter = require('events').EventEmitter;
var assign       = require('object-assign');
var Dispatcher   = require('../dispatcher');
var C            = require('../constants');

var CHANGE_EVENT = 'change';

const storeCallback = (payload) => {
    var action = payload.action;

    switch(action.actionType) {
        case C.NOTIFICATION_ADD:
            if(action.data) {
                _unread.push(action.data);
                Notifications.emitChange();
            }
            break;

        case C.NOTIFICATION_LOCK:
            _lock = action.data || true;
            Notifications.emitChange();
            break;

        case C.NOTIFICATION_UNLOCK:
            _lock = false;
            Notifications.emitChange();
            break;

    }

    return true;
};
Dispatcher.register(storeCallback);

var _notifications = [];
var _unread = [];
var _lock = false;

var Notifications = assign({}, EventEmitter.prototype, {

    getNew: function() {
        var msg = null;

        if(_unread.length > 0) {
            msg = _unread.shift();
            _notifications.push(msg);
        }

        return msg;
    },

    emitChange: function() {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener: function(callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    },

    getLock: () => {
        return _lock;
    }

});

module.exports = Notifications;