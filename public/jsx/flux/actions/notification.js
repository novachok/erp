var Dispatcher   = require('../dispatcher');
var C            = require('../constants');

var NotificationActions = {

    add: function(message, level) {
        Dispatcher.handleViewAction({
            actionType: C.NOTIFICATION_ADD,
            data: {
                message: message,
                level: level
            }
        });
    },

    lock: function(options) {
        Dispatcher.handleViewAction({
            actionType: C.NOTIFICATION_LOCK,
            data: options
        });
    },

    unlock: function() {
        Dispatcher.handleViewAction({
            actionType: C.NOTIFICATION_UNLOCK
        });
    }
};

module.exports = NotificationActions;