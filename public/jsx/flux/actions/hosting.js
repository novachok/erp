var Dispatcher   = require('../dispatcher');
var C            = require('../constants');

var HostingActions = {

    load: function() {
        Dispatcher.handleViewAction({
            actionType: C.HOSTING_LOAD
        });
    },

    create: function(data) {
        Dispatcher.handleViewAction({
            actionType: C.HOSTING_CREATE,
            data: data
        });
    },

    update: function(id, data) {
        Dispatcher.handleViewAction({
            actionType: C.HOSTING_UPDATE,
            id: id,
            data: data
        });
    },

    rm: function(id) {
        Dispatcher.handleViewAction({
            actionType: C.HOSTING_DELETE,
            id: id
        });
    }

};

module.exports = HostingActions;