var Dispatcher   = require('../dispatcher');
var C            = require('../constants');

var DocActions = {

    load: function() {
        Dispatcher.handleViewAction({
            actionType: C.DOC_LOAD
        });
    },

    create: function(data) {
        Dispatcher.handleViewAction({
            actionType: C.DOC_CREATE,
            data: data
        });
    },

    update: function(id, data) {
        Dispatcher.handleViewAction({
            actionType: C.DOC_UPDATE,
            id: id,
            data: data
        });
    },

    host: function(data) {
        Dispatcher.handleViewAction({
            actionType: C.DOC_PHONY,
            data: data
        });
    },

    rm: function(id) {
        Dispatcher.handleViewAction({
            actionType: C.DOC_DELETE,
            id: id
        });
    },

    changeItems: function(num) {
        Dispatcher.handleViewAction({
            actionType: C.DOC_CHANGE_ITEMS,
            num: num
        });
    }

};

module.exports = DocActions;