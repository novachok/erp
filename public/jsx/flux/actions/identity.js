var Dispatcher   = require('../dispatcher');
var C            = require('../constants');

var IdentityActions = {

    set: function(data) {
        Dispatcher.handleViewAction({
            actionType: C.IDENTITY_SET,
            data: data
        });
    },

    update: function(data) {
        Dispatcher.handleViewAction({
            actionType: C.IDENTITY_UPDATE,
            data: data
        });
    },

    load: function(data) {
        Dispatcher.handleViewAction({
            actionType: C.IDENTITY_LOAD,
            data: data
        });
    },

    unset: function() {
        Dispatcher.handleViewAction({
            actionType: C.IDENTITY_UNSET
        });
    }

};

module.exports = IdentityActions;