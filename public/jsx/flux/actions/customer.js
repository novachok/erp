var Dispatcher   = require('../dispatcher');
var C            = require('../constants');

var CustomerActions = {

    load: function() {
        Dispatcher.handleViewAction({
            actionType: C.CUSTOMER_LOAD
        });
    },

    create: function(data) {
        Dispatcher.handleViewAction({
            actionType: C.CUSTOMER_CREATE,
            data: data
        });
    },

    update: function(id, data) {
        Dispatcher.handleViewAction({
            actionType: C.CUSTOMER_UPDATE,
            id: id,
            data: data
        });
    },

    rm: function(id) {
        Dispatcher.handleViewAction({
            actionType: C.CUSTOMER_DELETE,
            id: id
        });
    }

};

module.exports = CustomerActions;