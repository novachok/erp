var NotificationAction = require('./flux/actions/notification');
var assign             = require('object-assign');
var _                  = require('../../lib/_');
var auth               = require('./auth');

var connect = function(){
    this.options = {};
    this.auth = auth.getInstance();
};

connect.prototype.onFailure = function(response) {

    if('responseJSON' in response) {
        var responseData = response.responseJSON;
    } else {
        var responseData = {
            success: false,
            error: {
                message: response.statusText
            }
        };

        switch (response.status) {
            case 404:
                responseData.error.message = "Wrong route or API didn't recognize it \n"+ response.responseText;
                break;
        }
    }

    NotificationAction.add(responseData.error.message, "error");
    if(_.isFunction(this.options.error)) {
        this.options.error(responseData);
    }
};

connect.prototype.onSuccess = function(response) {
    if(_.isFunction(this.options.success)) {
        this.options.success(response);
    }
    if('message' in response) {
        NotificationAction.add(response.message, "success");
    }
};

connect.prototype.onComplete = function(response) {
    if(this.options.lock) {
        NotificationAction.unlock();
    }
};

connect.prototype.defaultOptions = function() {
    return {
        dataType: "json",
        error: this.onFailure.bind(this),
        success: this.onSuccess.bind(this),
        complete: this.onComplete.bind(this)
    };
};

connect.prototype.makeData = function(data) {
    if(this.auth.getToken()) {
        data.token = this.auth.getToken();
    }

    return data;
};

connect.prototype.setOptions = function(options) {
    this.options = assign(this.options, {
        success: null,
        error: null,
        lock: false
    }, options);

    if(this.options.lock) {
        NotificationAction.lock({
            message: this.options.lock
        });
    }
};

connect.prototype.get = function(url, data, options) {
    this.setOptions(options);

    var _o = assign(this.defaultOptions(), {
        type: "GET",
        url: url,
        data: this.makeData(data)
    });

    return $.ajax(_o);
};

connect.prototype.post = function(url, data, options) {
    this.setOptions(options);

    var _o = assign(this.defaultOptions(), {
        type: "POST",
        url: url,
        data: this.makeData(data)
    });

    return $.ajax(_o);
};

connect.prototype.put = function(url, data, options) {
    this.setOptions(options);

    var _o = assign(this.defaultOptions(), {
        type: "PUT",
        url: url,
        data: this.makeData(data)
    });

    return $.ajax(_o);
};

connect.prototype.rm = function(url, data, options) {
    this.setOptions(options);

    var _o = assign(this.defaultOptions(), {
        type: "DELETE",
        url: url,
        data: this.makeData(data)
    });

    return $.ajax(_o);
};

module.exports = connect;