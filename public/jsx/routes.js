module.exports = [
    { path: '/reset/:id', component: require('./components/moduleProfile') },
    { path: '/customers', component: require('./components/moduleCustomers') },
    { path: '/customer/:action/:id', component: require('./components/moduleCustomers') },
    { path: '/hosting', component: require('./components/moduleHosting') },
    { path: '/hosting/:action/:id', component: require('./components/moduleHosting') },
    { path: '/docs', component: require('./components/moduleDocs') },
    { path: '/docs/p/:page', component: require('./components/moduleDocs') },
    { path: '/docs/:action/:id', component: require('./components/moduleDocs') },
    { path: '/me', component: require('./components/moduleProfile') }
];