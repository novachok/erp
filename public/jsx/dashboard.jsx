import { Link } from 'react-router'

var React             = require('react');
var IdentityStore     = require('./flux/store/identity');
var NotificationStore = require('./flux/store/notification');
var IdentityActions   = require('./flux/actions/identity');
var auth              = require('./auth').getInstance();

/**
 * Logout Popover to confirm action
 */
var LogoutPopover = React.createClass({
    displayName: 'logoutPopover',
    propTypes: {
        logout: React.PropTypes.func.isRequired
    },
    logout: function() {
        this.props.logout();
    },
    componentDidMount: function() {
        var self = this;
        $("#logout-popover-yes").click(function(){
            self.logout();
        });
        $("#logout-popover-no").click(function(){
            self.cancel();
        });
    },
    render: function(){
        return (
            <div id="logout-popover" className="hiddenElements">
                <h3>Are you sure to sign-out?</h3>
                <button type="button" className="btn btn-xs btn-primary" id="logout-popover-yes" onClick={this.logout}>Yes</button>
                <button type="button" className="btn btn-xs btn-default" id="logout-popover-no" onClick={this.cancel}>No</button>
            </div>
        );
    }
});

/**
 * Main Navigation bar
 */
var DashboardModule = React.createClass({
    displayName: 'dashboardModule',
    getInitialState: function() {
        return {
            user: IdentityStore.getUser(),
            mounted: false
        };
    },
    componentDidMount: function() {
        this.setState({mounted: true});
        IdentityStore.addChangeListener(this._onChange);
        NotificationStore.addChangeListener(this.notify);
    },
    componentWillUnmount: function() {
        this.setState({mounted: false});
        IdentityStore.removeChangeListener(this._onChange);
        NotificationStore.removeChangeListener(this.notify);
    },
    _onChange: function() {
        if(this.state.mounted) {
            this.setState({user: IdentityStore.getUser()});
        }
    },
    notify: function() {
        if(this.state.mounted) {
            var n = NotificationStore.getNew();
            if(n) {
                $("#notificationAnchor").notify(n.message, {
                    className: n.level,
                    position: 'bottom'
                });
            }

            var _l = NotificationStore.getLock();
            if(_l) {
                $.blockUI(_l);
            } else {
                $.unblockUI();
            }
        }
    },
    logout: function(){
        IdentityActions.unset();
        auth.rmToken();
    },
    dialog: function() {
        $.blockUI({
            message: $('#logout-popover'),
            css: { width: '300px' }
        });
    },
    render: function(){
        return (
            <div>
                <nav className="navbar navbar-toggleable-md navbar-dark navbar-inverse bg-inverse fixed-top">
                    <ul className="nav navbar-nav">
                        <li className="nav-item" id="notificationAnchor">
                            <Link className="nav-link" to="/" activeClassName="active" onlyActiveOnIndex>Dashboard</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" activeClassName="active" to="/customers">Customers</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" activeClassName="active" to="/hosting">Hosting</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" activeClassName="active" to="/docs">Docs</Link>
                        </li>
                    </ul>

                    <ul className="nav navbar-nav pull-right">
                        <li className="nav-item">
                            <Link className="nav-link" activeClassName="active" to="/me">{this.state.user.email}</Link>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" id="btn-logout" onClick={this.dialog}>
                                <i className="fa fa-sign-out"></i>
                            </a>
                        </li>
                    </ul>
                </nav>


                <div>
                    {this.props.children}
                </div>

                <div className="hiddenElements">
                    <LogoutPopover logout={this.logout} />
                </div>

            </div>
        );
    }
});

module.exports = DashboardModule;