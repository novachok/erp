import { Link } from 'react-router';

var React           = require('react');
var FormCustomer    = require('./formCustomer');
var Nl2Br           = require('./nl2br');
var CustomerStore   = require('../flux/store/customer');
var CustomerActions = require('../flux/actions/customer');
var _               = require('../../../lib/_');

var CustomerGridLine = React.createClass({
    displayName: 'customerGridLine',
    render: function() {
        var editUrl = '/customer/edit/'+ this.props.customer.id;
        var delUrl = '/customer/delete/'+ this.props.customer.id;
        return (
            <tr>
                <td>{this.props.customer.title}</td>
                <td>{this.props.customer.contact}</td>
                <td>{this.props.customer.email}</td>
                <td>{this.props.customer.phone}</td>
                <td>
                    <Nl2Br text={this.props.customer.notes} />
                </td>
                <td>
                    <Link className="btn btn-link" to={editUrl}>
                        <i className="fa fa-pencil"></i> Edit
                    </Link>
                    <Link className="btn btn-link" to={delUrl}>
                        <i className="fa fa-trash"></i> Delete
                    </Link>
                </td>
            </tr>
        );
    }
});

var moduleCustomers = React.createClass({
    displayName: 'moduleCustomers',
    getInitialState: function() {
        return {
            customers: CustomerStore.getAll()
        };
    },
    componentDidMount: function() {
        CustomerStore.addChangeListener(this._onChange);
    },
    componentWillUnmount: function() {
        CustomerStore.removeChangeListener(this._onChange);
    },
    _onChange: function() {
        this.setState({
            customers: CustomerStore.getAll()
        });
    },
    render: function(){
        var action = (this.props.params.action || null);
        if(action == 'add' || action == 'edit') {
            var customerId = _.isNumeric(this.props.params.id) ? this.props.params.id : null;
            return (
                <FormCustomer id={customerId}/>
            );
        } else {

            if(action == 'delete') {
                CustomerActions.rm(this.props.params.id);
            }

            if(this.state.customers.length > 0) {
                var grid = this.state.customers.map(function(customer) {
                    return (
                        <CustomerGridLine key={customer.id} customer={customer} />
                    );
                });
            } else {
                var grid = (
                    <tr>
                        <td colSpan="6" className="text-lg-center">There is no data</td>
                    </tr>
                );
            }

            return (
                <div>
                    <Link className="btn btn-secondary btn-sm" to="/customer/add/new">
                        <i className="fa fa-plus"></i> Add New
                    </Link>

                    <br />
                    <div>
                        <table className="table table-striped table-hover table-sm table-bordered">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Contact</th>
                                    <th>E-mail</th>
                                    <th>Phone</th>
                                    <th>Notes</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                {grid}
                            </tbody>
                        </table>
                    </div>
                </div>
            );
        }
    }
});

module.exports = moduleCustomers;