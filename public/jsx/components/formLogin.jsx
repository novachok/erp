import { Link } from 'react-router'

var React = require('react');

var AlertFailLogin = React.createClass({
    displayName: 'AlertFailLogin',
    render: function() {
        return (
            <div className="alert alert-warning" role="alert">
                {this.props.error}
            </div>
        );
    }
});

var FormLogin = React.createClass({
    displayName: 'FormLogin',
    getInitialState: function() {
        return {
            email: '',
            password: '',
            remember: false,
            reset: false
        };
    },
    handleEmailInput: function(e){
        this.setState({email: e.target.value});
    },
    handlePasswordInput: function(e){
        this.setState({password: e.target.value});
    },
    handleRememberMe: function(e){
        this.setState({remember: !!(e.target.checked)});
    },
    handleSubmit: function(e){
        e.preventDefault();
        if(!this.state.email || !this.state.password)
            return false;
        this.props.authorize(this.state.email, this.state.password, this.state.remember);
    },
    resetPassword: function(e) {
        if(e)
            e.preventDefault();
        this.setState({reset: !(this.state.reset)});
    },
    handleReset: function(e) {
        e.preventDefault();
        if(!this.state.email)
            return false;
        this.props.reset(this.state.email);
        this.resetPassword();
    },
    render: function(){
        var error = this.props.error ? <AlertFailLogin error={this.props.error} /> : '';
        if(this.state.reset) {
            return (
                <form className="form-login" onSubmit={this.handleReset}>
                    <h2 className="form-signin-heading">Reset password</h2>
                    <label htmlFor="inputEmail" className="sr-only">Email address</label>
                    <input type="email" id="inputEmail" className="form-control" placeholder="Email address" required
                           autoFocus onChange={this.handleEmailInput}/>
                    <button className="btn btn-lg btn-primary btn-block">Reset password</button>
                    <a className="btn btn-link" onClick={this.resetPassword}>Back to login</a>
                </form>
            );
        } else {
            return (
                <form className="form-login" onSubmit={this.handleSubmit}>
                    <h2 className="loginForm-heading">Please sign in</h2>
                    {error}
                    <label htmlFor="inputEmail" className="sr-only">Email address</label>
                    <input type="email" id="inputEmail" className="form-control" placeholder="Email address" required
                           autoFocus onChange={this.handleEmailInput}/>
                    <label htmlFor="inputPassword" className="sr-only">Password</label>
                    <input type="password" id="inputPassword" className="form-control" placeholder="Password" required
                           onChange={this.handlePasswordInput}/>
                    <div className="checkbox">
                        <label>
                            <input type="checkbox" onChange={this.handleRememberMe}/>
                            Remember me
                        </label>
                    </div>
                    <button className="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                    <a className="btn btn-link" onClick={this.resetPassword}>Reset password</a>
                </form>
            );
        }
    }
});

FormLogin.propTypes = {
    authorize: React.PropTypes.func.isRequired
};

module.exports = FormLogin;