var React = require('react');

var InputDate = React.createClass({
    displayName: 'InputDate',
    propTypes: {
        name: React.PropTypes.string.isRequired,
        change: React.PropTypes.func.isRequired,
        value: React.PropTypes.string
    },
    componentDidMount: function() {
        var self = this;
        $('input[name='+ this.props.name +']').Zebra_DatePicker({
            onSelect: function() {
                self.props.change({
                    target: {
                        name: this[0].name,
                        value: this[0].value
                    }
                });
            }
        });
    },
    value2string: function(){
        var v = '';
        if(this.props.value) {
            var d = new Date(this.props.value);
            var month = ('0'+ (d.getMonth()+1)).slice(-2);
            var day = ('0'+ d.getDate()).slice(-2);
            v = d.getFullYear() +'-'+ month +'-'+ day;
        }
        return v;
    },
    render: function(){
        return (
            <input className="form-control" name={this.props.name} id={this.props.name} onChange={this.props.change} value={this.value2string()} />
        );
    }
});

module.exports = InputDate;