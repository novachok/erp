var React = require('react');
var IdentityStore   = require('../flux/store/identity');
var IdentityActions = require('../flux/actions/identity');

var BankAccountItem = React.createClass({
    displayName: 'BankAccountItem',
    edit: function(e) {
        e.preventDefault();
        this.props.edit(this.props.item.id);
    },
    delete: function(e) {
        e.preventDefault();
        this.props.delete(this.props.item.id);
    },
    render: function() {
        var cls = this.props.item.id == this.props.selected ? 'table-info' : '';
        var def = this.props.item.isDefault ? (
            <i className="fa fa-check">

            </i>
        ) : '';
        return (
            <tr className={cls}>
                <td>{this.props.n}</td>
                <td>{this.props.item.title}</td>
                <td>{this.props.item.currency}</td>
                <td>{def}</td>
                <td className="text-nowrap">
                    <a href="#" onClick={this.edit}><i className="fa fa-pencil"></i> Edit</a> &nbsp;
                    <a href="#" onClick={this.delete}><i className="fa fa-ban"></i> Delete</a>
                </td>
            </tr>
        );
    }
});

var FormProfile = React.createClass({
    displayName: 'FormProfile',
    getInitialState: function() {
        return {
            user: IdentityStore.getUser(),
            account: this._getEmptyAccount(),
            newAccountId: -1
        };
    },
    componentDidMount: function() {
        IdentityStore.addChangeListener(this._onChange);
        $("#profile-form").validate({
            rules: {
                password_confirm: { equalTo: '#password' }
            }
        });
    },
    componentWillUnmount: function() {
        IdentityStore.removeChangeListener(this._onChange);
    },
    _onChange: function() {
        this.setState({user: IdentityStore.getUser()});
    },
    handleSubmit: function(e) {
        e.preventDefault();
        var data = this.state;
        if($("#password").val()) {
            data.user.password = $("#password").val();
        }
        IdentityActions.update(data.user);
    },
    fieldChange: function(e) {
        var _state = {};
        _state[e.target.name] = e.target.value;
        this.setState({user: _state});
    },
    lineChange: function(e) {
        var _account = this.state.account;
        _account[e.target.name] = e.target.value;
        this.setState({account: _account});
    },
    _getEmptyAccount: function() {
        return {
            id: 0,
            title: '',
            currency: '',
            mfo: '',
            account: '',
            isDefault: false
        };
    },
    addAccount: function(e) {
        e ? e.preventDefault() : null;
        var _account = this.state.account;
        var _user    = this.state.user;

        // Unset all defaults
        if(_account.isDefault) {
            for(var i=0; i<_user.accounts.length; i++) {
                _user.accounts[i].isDefault = false;
            }
        }

        if(_account.id != 0) {
            for(var i=0; i<_user.accounts.length; i++) {
                if(_account.id == _user.accounts[i].id) {
                    _user.accounts[i] = _account;
                    this.setState({user: _user, account: this._getEmptyAccount()});
                    return;
                }
            }
        }

        _account.id = this.state.newAccountId;
        _user.accounts.push(_account);
        this.setState({user: _user, account: this._getEmptyAccount(), newAccountId: (_account.id - 1)});
        return;
    },
    editAccount: function(id) {
        var _account = this._getEmptyAccount();
        for(var i=0; i<this.state.user.accounts.length; i++) {
            if(this.state.user.accounts[i].id == id) {
                _account = this.state.user.accounts[i];
            }
        }
        this.setState({account: _account});
    },
    cancelAccount: function(e) {
        e.preventDefault();
        this.setState({account: this._getEmptyAccount()});
    },
    deleteAccount: function(id) {
        var _user = this.state.user;
        for(var i=0; i<_user.accounts.length; i++) {
            if(_user.accounts[i].id == id) {
                _user.accounts.splice(i, 1);
            }
        }
        this.setState({user: _user});
    },
    render: function() {
        var self = this;
        var itemRows = this.state.user.accounts.map(function(item, i) {
                return (
                    <BankAccountItem key={item.id} n={i+1} id={item.id} item={item} edit={self.editAccount} delete={self.deleteAccount} selected={self.state.account.id} />
                );
            }) || '';

        return (
            <form className="erp-form erp-form-xl" id="profile-form" onSubmit={this.handleSubmit}>
                <div className="row">
                    <h2>Profile</h2>
                </div>
                <div className="row">
                    <div className="col-lg-6">
                        <div className="form-group">
                            <lable htmlFor="email">E-mail</lable>
                            <input type="email" name="email" id="email" className="form-control" required="required" value={this.state.user.email} onChange={this.fieldChange} />
                        </div>
                    </div>
                    <div className="col-lg-6">
                        <div className="row">
                            <div className="col-md-6 form-group">
                                <label htmlFor="password">Password</label>
                                <input type="password" id="password" name="password" className="form-control" />
                            </div>
                            <div className="col-md-6 form-group">
                                <label htmlFor="password_confirm">Confirm password</label>
                                <input type="password" id="password_confirm" name="password_confirm" className="form-control" />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-6">
                        <div className="form-group">
                            <label htmlFor="firstName">First Name</label>
                            <input type="text" name="firstName" id="firstName" className="form-control" value={this.state.user.firstName} onChange={this.fieldChange} />
                        </div>
                    </div>
                    <div className="col-lg-6">
                        <div className="form-group">
                            <label htmlFor="lastName">Last Name</label>
                            <input type="text" name="lastName" id="lastName" className="form-control" value={this.state.user.lastName} onChange={this.fieldChange} />
                        </div>
                    </div>
                </div>
                <hr />
                <div className="row">
                    <div className="col-lg-6">
                        <div className="form-group">
                            <label htmlFor="title">Title</label>
                            <input type="text" name="title" id="title" className="form-control" value={this.state.user.title} onChange={this.fieldChange} />
                        </div>
                    </div>
                    <div className="col-lg-6">
                        <div className="form-group">
                            <label htmlFor="inn">INN</label>
                            <input type="text" name="inn" id="inn" className="form-control" value={this.state.user.inn} onChange={this.fieldChange} />
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-6">
                        <div className="form-group">
                            <label htmlFor="phone">Phone</label>
                            <input type="text" name="phone" id="phone" className="form-control" value={this.state.user.phone} onChange={this.fieldChange} />
                        </div>
                    </div>
                    <div className="col-lg-6">
                        <div className="form-group">
                            <label htmlFor="address">Address</label>
                            <input type="text" name="address" id="address" className="form-control" value={this.state.user.address} onChange={this.fieldChange} />
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="form-group">
                        <table className="table table-sm table-items">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th width="60%">Bank</th>
                                <th>Currency</th>
                                <th>is Default</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            {itemRows}
                            </tbody>
                        </table>
                    </div>

                    <div className="row erp-form-inline">
                        <div className="col-lg-4">
                            <fieldset className="form-group">
                                <label htmlFor="title">Bank title</label>
                                <input type="text" className="form-control" name="title" value={this.state.account.title} onChange={this.lineChange} />
                            </fieldset>
                        </div>
                        <div className="col-lg-2">
                            <fieldset className="form-group">
                                <label htmlFor="account">Account</label>
                                <input type="text" className="form-control" name="account" value={this.state.account.account} onChange={this.lineChange} />
                            </fieldset>
                        </div>
                        <div className="col-lg-2">
                            <fieldset className="form-group">
                                <label htmlFor="mfo">MFO</label>
                                <input type="text" className="form-control" name="mfo" value={this.state.account.mfo} onChange={this.lineChange} />
                            </fieldset>
                        </div>
                        <div className="col-lg-1">
                            <fieldset className="form-group">
                                <label htmlFor="currency">Currency</label>
                                <select name="currency" className="form-control" value={this.state.account.currency} onChange={this.lineChange}>
                                    <option value=""> --- </option>
                                    <option value="UAH">UAH</option>
                                    <option value="USD">USD</option>
                                    <option value="EUR">EUR</option>
                                </select>
                            </fieldset>
                        </div>
                        <div className="col-lg-1">
                            <fieldset className="form-group checkbox">
                                <label>
                                    <input type="checkbox" name="isDefault" value="1" checked={this.state.account.isDefault} onChange={this.lineChange} />
                                    Def
                                </label>
                            </fieldset>
                        </div>
                        <div className="col-lg-2">
                            <label>&nbsp;</label><br />
                            <button className="btn btn-info" onClick={this.addAccount}><i className="fa fa-floppy-o"></i></button>
                            <button className="btn btn-secondary m-l-05" onClick={this.cancelAccount}><i className="fa fa-ban"></i></button>
                        </div>
                    </div>
                </div>

                <button className="btn btn-sm btn-primary" type="submit">Save</button>
            </form>
        );
    }
});

module.exports = FormProfile;