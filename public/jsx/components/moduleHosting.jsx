import { Link } from 'react-router';

var React          = require('react');
var FormHosting    = require('./formHosting');
var _              = require('../../../lib/_');
var HostingStore   = require('../flux/store/hosting');
var HostingActions = require('../flux/actions/hosting');

var HostingGridLine = React.createClass({
    displayName: 'HostingGridLine',
    render: function() {
        var editUrl = '/hosting/edit/'+ this.props.host.id;
        var delUrl = '/hosting/delete/'+ this.props.host.id;
        var docUrl = '/docs/host/'+ this.props.host.id;
        var Customer = '';
        if(this.props.host.customer) {
            var path = '/customer/edit/'+ this.props.host.customer.id;
            Customer = (
                <Link to={path}>{this.props.host.customer.title}</Link>
            );
        }
        var tickHosting = '';
        if(this.props.host.onlyHosting) {
            tickHosting = (
                <i className="fa fa-check-square-o"></i>
            );
        }
        var tickDomain = '';
        if(this.props.host.onlyDomain) {
            tickDomain = (
                <i className="fa fa-check-square-o"></i>
            );
        }

        var _now = new Date();
        var _10 = new Date(this.props.host.validTo);
        _10.setDate(_10.getDate() - 10);
        var _M = new Date(this.props.host.validTo);
        _M.setMonth(_M.getMonth() - 1);
        var cls = _10.getTime() < _now.getTime() ? 'alert alert-danger' : (_M.getTime() < _now.getTime() ? 'alert alert-warning' : '');

        return (
            <tr className={cls}>
                <td>{this.props.host.account}</td>
                <td>
                    {Customer}
                </td>
                <td>{this.props.host.domain}</td>
                <td>{this.props.host.validTo}</td>
                <td>{this.props.host.space}</td>
                <td className="text-lg-center">{tickHosting}</td>
                <td className="text-lg-center">{tickDomain}</td>
                <td>
                    Created: <i>{dateFormat('%d.%m.%Y %H:%i', new Date(this.props.host.createdAt))}</i>
                    <br />
                    {this.props.host.notes}
                </td>
                <td>
                    <Link className="btn btn-link" to={editUrl}>
                        <i className="fa fa-pencil"></i> Edit
                    </Link>
                    <Link className="btn btn-link" to={delUrl}>
                        <i className="fa fa-trash"></i> Delete
                    </Link>
                    <Link className="btn btn-link" to={docUrl}>
                        <i className="fa fa-file"></i> Invoice
                    </Link>
                </td>
            </tr>
        );
    }
});

var moduleHosting = React.createClass({
    displayName: 'moduleHosting',
    getInitialState: function() {
        return {
            hosts: HostingStore.getAll()
        };
    },
    componentDidMount: function() {
        HostingStore.addChangeListener(this._onChange);
    },
    componentWillUnmount: function() {
        HostingStore.removeChangeListener(this._onChange);
    },
    _onChange: function() {
        this.setState({
            hosts: HostingStore.getAll()
        });
    },
    render: function(){
        var action = (this.props.params.action || null);
        if(action == 'add' || action == 'edit') {
            var hostingId = _.isNumeric(this.props.params.id) ? this.props.params.id : null;
            return (
                <FormHosting id={hostingId}/>
            );
        } else {

            if (action == 'delete') {
                HostingActions.rm(this.props.params.id);
            }

            if (this.state.hosts.length > 0) {
                var grid = this.state.hosts.map(function (host) {
                    return (
                        <HostingGridLine key={host.id} host={host}/>
                    );
                });
            } else {
                var grid = (
                    <tr>
                        <td colSpan="9" className="text-lg-center">There is no data</td>
                    </tr>
                );
            }

            return (
                <div>
                    <Link className="btn btn-secondary btn-sm" to="/hosting/add/new">
                        <i className="fa fa-plus"></i> Add New
                    </Link>

                    <br />
                    <div>
                        <table className="table table-striped table-hover table-sm table-bordered">
                            <thead>
                            <tr>
                                <th>Account</th>
                                <th>Customer</th>
                                <th>Domain</th>
                                <th>Valid to</th>
                                <th>Space</th>
                                <th>1/h</th>
                                <th>1/d</th>
                                <th>Notes</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            {grid}
                            </tbody>
                        </table>
                    </div>
                </div>
            );
        }
    }
});

module.exports = moduleHosting;