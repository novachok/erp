var React = require('react');

var SelectItems = React.createClass({
    displayName: 'SelectItems',
    propTypes: {
        items: React.PropTypes.number.isRequired,
        change: React.PropTypes.func.isRequired
    },
    onChange: function(e) {
        this.props.change(e.target.value)
    },
    render: function() {
        return (
            <select className="form-control form-control-sm" value={this.props.items} onChange={this.onChange}>
                <option value="15">15</option>
                <option value="30">30</option>
                <option value="60">60</option>
                <option value="120">120</option>
            </select>
        );
    }
});


module.exports = SelectItems;