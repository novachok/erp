var React          = require('react');

var SelectEntity = React.createClass({
    displayName: 'SelectEntity',
    propTypes: {
        options: React.PropTypes.array.isRequired,
        change: React.PropTypes.func.isRequired,
        name: React.PropTypes.string.isRequired
    },
    getDefaultProps: function() {
        return {
            options: [],
            required: false,
            value: ""
        };
    },
    render: function() {
        var options = this.props.options.map(function(item) {
            return (
                <option key={item.id} value={item.id}>{item.title}</option>
            );
        });
        return (
            <select className="form-control" name={this.props.name} id={this.props.name} onChange={this.props.change} required={this.props.required} value={this.props.value}>
                <option value=""> --- </option>
                {options}
            </select>
        );
    }
});

module.exports = SelectEntity;