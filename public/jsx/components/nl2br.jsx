var React = require('react');
var newlineRegex = /(\r\n|\n\r|\r|\n)/g;

var Nl2Br = React.createClass({
    displayName: 'Nl2Br',
    render: function() {
        var texts = this.props.text.split(newlineRegex).map(function(line, index) {
            if(line.match(newlineRegex)) {
                return React.createElement('br', {key: index});
            } else {
                return line;
            }
        });

        return (
            <span>{texts}</span>
        );
    }
});

module.exports = Nl2Br;