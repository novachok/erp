import { Link } from 'react-router';

var React         = require('react');
var FormDoc       = require('./formDoc');
var SelectItems   = require('./selectItems');
var Pagination    = require('./pagination');
var _             = require('../../../lib/_');
var DocsStore     = require('../flux/store/doc');
var DocsActoins   = require('../flux/actions/doc');
var HostStore     = require('../flux/store/hosting');
var Pdf           = require('../pdf');
var C             = require('../flux/constants');


var DocGridLine = React.createClass({
    displayName: 'DocGridLine',
    pdfInvoice: function() {
        if(this.props.doc.isInvoiced) {
            Pdf.getInvoice(this.props.doc);
        }
        return null;
    },
    pdfAct: function() {
        if(this.props.doc.isActed) {
            Pdf.getAct(this.props.doc);
        }
        return null;
    },
    render: function(){
        var editUrl = '/docs/edit/'+ this.props.doc.id;
        var delUrl = '/docs/delete/'+ this.props.doc.id;
        var Customer = '';
        if(this.props.doc.customer) {
            var path = '/customer/edit/'+ this.props.doc.customer.id;
            Customer = (
                <Link to={path}>{this.props.doc.customer.title}</Link>
            );
        }
        var invoiced = this.props.doc.isInvoiced ? dateFormat('%d.%m.%Y', new Date(this.props.doc.dateOf)) : '';
        var acted    = this.props.doc.isActed    ? dateFormat('%d.%m.%Y', new Date(this.props.doc.actOf))  : '';
        var price = 0;

        if(this.props.doc.items) {
            for(var i=0; i<this.props.doc.items.length; i++) {
                price += this.props.doc.items[i].units * this.props.doc.items[i].price;
            }
        }
        price = price.toFixed(2);

        var pdf_inv = '';
        if(this.props.doc.isInvoiced) {
            pdf_inv = (
                <button className="btn btn-sm btn-secondary" onClick={this.pdfInvoice}><i className="fa fa-file-pdf-o"></i> Invoice</button>
            );
        }

        var pdf_act = '';
        if(this.props.doc.isActed) {
            pdf_act = (
                <button className="btn btn-sm btn-secondary" onClick={this.pdfAct}><i className="fa fa-file-pdf-o"></i> Act</button>
            );
        }

        return (
            <tr>
                <td className="text-lg-center">{this.props.doc.num}</td>
                <td>{invoiced}</td>
                <td>{acted}</td>
                <td className="text-lg-right">{price}</td>
                <td>{Customer}</td>
                <td>{dateFormat('%d.%m.%Y %H:%i', new Date(this.props.doc.createdAt))}</td>
                <td>
                    <Link className="btn btn-link" to={editUrl}>
                        <i className="fa fa-pencil"></i> Edit
                    </Link>
                    <Link className="btn btn-link" to={delUrl}>
                        <i className="fa fa-trash"></i> Delete
                    </Link>
                    {pdf_inv}
                    {pdf_act}
                </td>
            </tr>
        );
    }
});

var moduleDocs = React.createClass({
    displayName: 'moduleDocs',
    getInitialState: function() {
        var page = this.getPage();
        return {
            itemsOnPage: DocsStore.itemsOnPage(),
            itemsTotal: DocsStore.itemsTotal(),
            docs: DocsStore.getAll(page)
        };
    },
    componentDidMount: function() {
        DocsStore.addChangeListener(this._onChange);
    },
    componentWillUnmount: function() {
        DocsStore.removeChangeListener(this._onChange);
    },
    _onChange: function() {
        var page = this.getPage();
        this.setState({
            itemsOnPage: DocsStore.itemsOnPage(),
            itemsTotal: DocsStore.itemsTotal(),
            docs: DocsStore.getAll(page)
        });
    },
    _changePage: function(page) {
        this.setState({
            docs: DocsStore.getAll(page)
        });
    },
    updateItemsOnPage: function(items) {
        DocsActoins.changeItems(items);
    },
    getPage: function() {
        return this.props.params.page ? this.props.params.page * 1 : 1;
    },
    render: function(){
        var action = (this.props.params.action || null);

        if(action == 'add' || action == 'edit') {
            var docId = _.isNumeric(this.props.params.id) ? this.props.params.id : null;
            return (
                <FormDoc id={docId}/>
            );
        } else if(action == 'host') {
            // Create fake doc and then edit it
            var _h = HostStore.get(this.props.params.id);
            var _now = new Date();
            var _d = {
                id: null,
                isInvoiced: true,
                dateOf: _now.toDateString(),
                customerId: _h.customerId,
                customer: _h.customer,
                items: [{
                    id: -1,
                    job: C.STR_DOC_ITEM_HOSTING
                }]
            };
            DocsActoins.host(_d);
            return (
                <FormDoc id="-1" item="0" />
            );
        } else {

            if (this.state.docs.length > 0) {
                var grid = this.state.docs.map(function (doc) {
                    return (
                        <DocGridLine key={doc.id} doc={doc}/>
                    );
                });
            } else {
                var grid = (
                    <tr>
                        <td colSpan="7" className="text-lg-center">There is no data</td>
                    </tr>
                );
            }

            var page = this.getPage();

            return (
                <div>
                    <Link className="btn btn-secondary btn-sm" to="/docs/add/new">
                        <i className="fa fa-plus"></i> Add Doc
                    </Link>
                    <br />

                    <div>
                        <table className="table table-striped table-hover table-sm table-bordered">
                            <thead>
                            <tr>
                                <th className="text-lg-center">Num</th>
                                <th>Invoiced</th>
                                <th>Acted</th>
                                <th className="text-lg-right">Price</th>
                                <th>Customer</th>
                                <th>Created At</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                                {grid}
                            </tbody>
                        </table>
                    </div>

                    <div className="row form-inline">
                        <div className="col-lg-2 form-group">
                            <label className="form-control-label form-control-sm">Items on page</label> &nbsp;
                            <SelectItems items={this.state.itemsOnPage} change={this.updateItemsOnPage} />
                        </div>
                        <div className="col-lg-8">
                            <Pagination page={page} total={this.state.itemsTotal} item={this.state.itemsOnPage} change={this._changePage} />
                        </div>
                        <div className="col-lg-2 text-sm-right">
                            <label className="form-control-label form-control-sm"><b>{this.state.docs.length}</b> items of <b>{this.state.itemsTotal}</b> shown</label>
                        </div>
                    </div>
                </div>
            );
        }
    }
});

module.exports = moduleDocs;