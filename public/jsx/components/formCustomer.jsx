var React = require('react');
var CustomerStore   = require('../flux/store/customer');
var CustomerActions = require('../flux/actions/customer');

var FormCustomer = React.createClass({
    displayName: 'FormCustomer',
    getInitialState: function() {
        var customerId = this.props.id || null;
        return CustomerStore.getOrEmpty(customerId);
    },
    componentDidMount: function() {
        CustomerStore.addChangeListener(this._onChange);
    },
    componentWillUnmount: function() {
        CustomerStore.removeChangeListener(this._onChange);
    },
    _onChange: function() {
        var customerId = this.props.id || null;
        this.setState(CustomerStore.get(customerId));
    },
    handleSubmit: function(e) {
        e.preventDefault();
        var data = this.state;
        if(data.id) {
            CustomerActions.update(data.id, data);
        } else {
            CustomerActions.create(data);
        }
    },
    fieldChange: function(e) {
        var _state = {};
        _state[e.target.name] = e.target.value;
        this.setState(_state);
    },
    render: function() {
        return (
            <form className="erp-form" id="customer-form" onSubmit={this.handleSubmit}>
                <h2>Customer</h2>
                <fieldset className="form-group">
                    <label htmlFor="title">Title</label>
                    <input type="text" name="title" id="title" className="form-control" required="required" value={this.state.title} onChange={this.fieldChange} />
                </fieldset>
                <fieldset className="form-group">
                    <label htmlFor="contact">Contact Person</label>
                    <input type="text" name="contact" id="contact" className="form-control" value={this.state.contact} onChange={this.fieldChange} />
                </fieldset>
                <fieldset className="form-group">
                    <label htmlFor="email">E-mail</label>
                    <input type="email" name="email" id="email" className="form-control" value={this.state.email} onChange={this.fieldChange} />
                </fieldset>
                <fieldset className="form-group">
                    <label htmlFor="phone">Phone</label>
                    <input type="text" name="phone" id="phone" className="form-control" value={this.state.phone} onChange={this.fieldChange} />
                </fieldset>
                <fieldset className="form-group">
                    <label htmlFor="notes">Notes</label>
                    <textarea name="notes" id="notes" className="form-control" onChange={this.fieldChange} value={this.state.notes} />
                </fieldset>
                <button className="btn btn-sm btn-primary" type="submit">Save</button>
            </form>
        );
    }
});

module.exports = FormCustomer;