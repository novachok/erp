var React          = require('react');
var HostingActions = require('../flux/actions/hosting');
var HostingStore   = require('../flux/store/hosting');
var CustomerStore  = require('../flux/store/customer');
var SelectEntity   = require('./selectEntity');
var InputDate      = require('./inputDate');


var FormHosting = React.createClass({
    displayName: 'FormHosting',
    getInitialState: function() {
        var hostingId = this.props.id || null;
        return {
            hosting: HostingStore.getOrEmpty(hostingId),
            customers: CustomerStore.getAll()
        };
    },
    componentDidMount: function() {
        HostingStore.addChangeListener(this._onChange);
        CustomerStore.addChangeListener(this._onCustomers);
    },
    componentWillUnmount: function() {
        HostingStore.removeChangeListener(this._onChange);
        CustomerStore.removeChangeListener(this._onCustomers);
    },
    _onChange: function () {
        var hostingId = this.props.id || null;
        var _state = this.state;
        _state.hosting = HostingStore.get(hostingId);
        this.setState(_state);
    },
    _onCustomers: function() {
        var _state = this.state;
        _state.customers = CustomerStore.getAll();
        this.setState(_state);
    },
    fieldChange: function(e) {
        var _state = this.state.hosting;
        _state[e.target.name] = e.target.value;
        this.setState({hosting: _state});
    },
    checkboxChange: function(e) {
        var _state = this.state.hosting;
        _state[e.target.name] = !!(e.target.checked);
        this.setState({hosting: _state});
    },
    handleSubmit: function(e) {
        e.preventDefault();
        var data = this.state.hosting;
        if(data.id) {
            HostingActions.update(data.id, data);
        } else {
            HostingActions.create(data);
        }
    },
    render: function() {
        return (
            <form className="erp-form" id="hosting-form" onSubmit={this.handleSubmit}>
                <h2>Hosting account</h2>
                <fieldset className="form-group">
                    <label htmlFor="customerId">Customer</label>
                    <SelectEntity options={this.state.customers} change={this.fieldChange} name="customerId" required={true} value={this.state.hosting.customerId} />
                </fieldset>
                <fieldset className="form-group">
                    <label htmlFor="title">Account login</label>
                    <input type="text" name="account" id="title" className="form-control" required="required" value={this.state.hosting.account} onChange={this.fieldChange} />
                </fieldset>
                <fieldset className="form-group">
                    <label htmlFor="domain">Domain</label>
                    <input type="text" name="domain" id="domain" className="form-control" required="required" value={this.state.hosting.domain} onChange={this.fieldChange} />
                </fieldset>
                <fieldset className="form-group">
                    <label htmlFor="space">HDD space</label>
                    <input type="text" name="space" id="space" className="form-control" value={this.state.hosting.space} onChange={this.fieldChange} />
                </fieldset>
                <fieldset className="form-group">
                    <label htmlFor="validTo">Valid to</label>
                    <InputDate name="validTo" change={this.fieldChange} value={this.state.hosting.validTo} />
                </fieldset>
                <div className="checkbox">
                    <label>
                        <input type="checkbox" name="onlyHosting" checked={this.state.hosting.onlyHosting} onChange={this.checkboxChange} />
                        Only Hosting
                    </label>
                </div>
                <div className="checkbox">
                    <label>
                        <input type="checkbox" name="onlyDomain" checked={this.state.hosting.onlyDomain} onChange={this.checkboxChange} />
                        Only Domain
                    </label>
                </div>
                <div className="checkbox">
                    <label>
                        <input type="checkbox" name="isOff" checked={this.state.hosting.isOff} onChange={this.checkboxChange} />
                        Hosting &amp; Domain disabled
                    </label>
                </div>
                <fieldset className="form-group">
                    <label htmlFor="notes">Notes</label>
                    <textarea name="notes" className="form-control" value={this.state.hosting.notes} onChange={this.fieldChange} />
                </fieldset>
                <button className="btn btn-sm btn-primary" type="submit">Save</button>
            </form>
        );
    }
});

module.exports = FormHosting;