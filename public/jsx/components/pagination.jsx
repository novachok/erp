import { Link } from 'react-router';
var React = require('react');

var Pagination = React.createClass({
    displayName: 'Pagination',
    propTypes: {
        page: React.PropTypes.number.isRequired,
        item: React.PropTypes.number.isRequired,
        total: React.PropTypes.number.isRequired,
        change: React.PropTypes.func.isRequired
    },
    changePage: function(e) {
        var page = $(e.target).data("page")*1;
        this.props.change(page);
    },
    getDefaultProps: function() {
        return {
            page: 1,
            item: 15,
            total: 0
        };
    },
    render: function() {

        var pages = Math.ceil(this.props.total / this.props.item);

        var prev = '';
        if(this.props.page > 1) {
            var url = '/docs/p/'+ (this.props.page - 1);
            prev = (
                <li className="page-item">
                    <Link className="page-link" to={url} aria-label="Previous" onClick={this.changePage} data-page={this.props.page - 1}>
                        <span aria-hidden="true">&laquo;</span>
                        <span className="sr-only">Previous</span>
                    </Link>
                </li>
            );
        }

        var next = '';
        if(this.props.page < pages) {
            var url = '/docs/p/'+ (this.props.page + 1);
            next = (
                <li className="page-item">
                    <Link className="page-link" to={url} aria-label="Next" onClick={this.changePage} data-page={this.props.page + 1}>
                        <span aria-hidden="true">&raquo;</span>
                        <span className="sr-only">Next</span>
                    </Link>
                </li>
            );
        }

        var links = [];
        for(var i=1; i<=pages; i++) {
            var l = '';
            var url = '/docs/p/' + i;
            if(i == this.props.page) {
                l = (
                    <li className="page-item active" key={i}>
                        <Link className="page-link" to={url}>{i}</Link>
                    </li>
                );
            } else {
                l = (
                    <li className="page-item" key={i}>
                        <Link className="page-link" onClick={this.changePage} to={url} data-page={i}>{i}</Link>
                    </li>
                );
            }
            links.push(l);
        }

        return (
            <nav className="text-md-center">
                <ul className="pagination pagination-sm">
                    {prev}
                    {links}
                    {next}
                </ul>
            </nav>
        );
    }
});

module.exports = Pagination;