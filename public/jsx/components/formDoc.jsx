var React          = require('react');
var SelectEntity   = require('./selectEntity');
var InputDate      = require('./inputDate');
var CustomerStore  = require('../flux/store/customer');
var DocStore       = require('../flux/store/doc');
var DocActions     = require('../flux/actions/doc');
var IdentityStore  = require('../flux/store/identity');

var DocItem = React.createClass({
    displayName: 'DocItem',
    propTypes: {
        id: React.PropTypes.number.isRequired,
        item: React.PropTypes.object.isRequired,
        edit: React.PropTypes.func.isRequired,
        delete: React.PropTypes.func.isRequired
    },
    getDefaultProps: function() {
        return {
            selected: 0
        };
    },
    totalValue: function() {
        var u = this.props.item.units || 0;
        var p = this.props.item.price || 0;
        return u * p;
    },
    edit: function(e) {
        e.preventDefault();
        this.props.edit(this.props.item.id);
    },
    delete: function(e) {
        e.preventDefault();
        this.props.delete(this.props.item.id);
    },
    render: function() {
        var cls = this.props.item.id == this.props.selected ? 'table-info' : '';
        return (
            <tr className={cls}>
                <td>{this.props.n}</td>
                <td>{this.props.item.job}</td>
                <td>{this.props.item.units}</td>
                <td>{this.props.item.price}</td>
                <td>{this.props.item.units * this.props.item.price}</td>
                <td className="text-nowrap">
                    <a href="#" onClick={this.edit}><i className="fa fa-pencil"></i> Edit</a> &nbsp;
                    <a href="#" onClick={this.delete}><i className="fa fa-ban"></i> Delete</a>
                </td>
            </tr>
        );
    }
});


var FormDoc = React.createClass({
    displayName: 'FormDoc',
    getDefaultProps: function() {
        return {
            item: null
        };
    },
    getInitialState: function() {
        var docId = this.props.id || null;
        var _d    = DocStore.getOrEmpty(docId);
        return {
            newItemId: -1,
            item: this.props.item !== null && this.props.item in _d.items ? _d.items[this.props.item] : this._getEmptyItem(),
            doc: _d,
            customers: CustomerStore.getAll()
        };
    },
    _getEmptyItem: function() {
        return {
            id: 0,
            job: '',
            units: '',
            price: ''
        };
    },
    componentDidMount: function() {
        DocStore.addChangeListener(this._onChange);
        CustomerStore.addChangeListener(this._onCustomers);
    },
    componentWillUnmount: function() {
        DocStore.removeChangeListener(this._onChange);
        CustomerStore.removeChangeListener(this._onCustomers);
    },
    _onChange: function () {
        var docId = this.props.id || null;
        var _state = this.state;
        _state.doc = DocStore.get(docId);
        this.setState(_state);
    },
    getAccounts: function(){
        var _a = [];
        var accounts = IdentityStore.getAccounts();
        for(var x in accounts) {
            _a.push({
                id: accounts[x].id,
                title: accounts[x].title +' ['+ accounts[x].currency +']'
            });
        }
        return _a;
    },
    _onCustomers: function() {
        var _state = this.state;
        _state.customers = CustomerStore.getAll();
        this.setState(_state);
    },
    fieldChange: function(e) {
        var _state = this.state.doc;
        _state[e.target.name] = e.target.value;
        this.setState({doc: _state});
    },
    checkboxChange: function(e) {
        var _state = this.state.doc;
        _state[e.target.name] = !!(e.target.checked);
        this.setState({doc: _state});
    },
    lineChange: function(e) {
        var _state = this.state.item;
        _state[e.target.name] = e.target.value;
        this.setState({item: _state});
    },
    handleSubmit: function(e) {
        e.preventDefault();
        var data = this.state.doc;
        var _item = this.state.item;
        if(_item.job && _item.units && _item.price) {
            this.addItem();
        }

        if(data.id) {
            DocActions.update(data.id, data);
        } else {
            DocActions.create(data);
        }
    },
    addItem: function(e) {
        e ? e.preventDefault() : null;
        var _item = this.state.item;
        var _doc  = this.state.doc;

        if(_item.id != 0) {
            for(var i=0; i<_doc.items.length; i++) {
                if(_item.id == _doc.items[i].id) {
                    _doc.items[i] = _item;
                    this.setState({doc: _doc, item: this._getEmptyItem()});
                    return;
                }
            }
        }

        _item.id = this.state.newItemId;
        _doc.items.push(_item);
        this.setState({doc: _doc, item: this._getEmptyItem(), newItemId: (_item.id - 1)});
        return;
    },
    cancelItem: function(e) {
        e.preventDefault();
        this.setState({item: this._getEmptyItem()});
    },
    editItem: function(id) {
        var _item = this._getEmptyItem();
        for(var i=0; i<this.state.doc.items.length; i++) {
            if(this.state.doc.items[i].id == id) {
                _item = this.state.doc.items[i];
            }
        }
        this.setState({item: _item});
    },
    deleteItem: function(id) {
        var _doc = this.state.doc;
        for(var i=0; i<_doc.items.length; i++) {
            if(_doc.items[i].id == id) {
                _doc.items.splice(i, 1);
            }
        }
        this.setState({doc: _doc});
    },
    render: function () {
        var self = this;
        var itemRows = this.state.doc.items.map(function(item, i) {
            return (
                <DocItem key={item.id} n={i+1} id={item.id} item={item} edit={self.editItem} delete={self.deleteItem} selected={self.state.item.id} />
            );
        }) || '';
        var total = 0;
        for(var i=0; i<this.state.doc.items.length; i++) {
            total = total*1 + this.state.doc.items[i].units * this.state.doc.items[i].price;
        }

        return (
            <form className="erp-form erp-form-lg" id="doc-form" onSubmit={this.handleSubmit}>
                <h2>Document</h2>
                <div className="row">
                    <div className="col-lg-6">
                        <div className="checkbox">
                            <label>
                                <input className="m-r-05" type="checkbox" name="isInvoiced" checked={this.state.doc.isInvoiced} onChange={this.checkboxChange} />
                                Is Invoiced
                            </label>
                        </div>
                        <fieldset className="form-group">
                            <label htmlFor="dateOf">Invoice Date</label>
                            <InputDate name="dateOf" change={this.fieldChange} value={this.state.doc.dateOf} />
                        </fieldset>
                    </div>
                    <div className="col-lg-6">
                        <div className="checkbox">
                            <label>
                                <input className="m-r-05" type="checkbox" name="isActed" checked={this.state.doc.isActed} onChange={this.checkboxChange} />
                                Is Acted
                            </label>
                        </div>
                        <fieldset className="form-group">
                            <label htmlFor="dateOf">Act Date</label>
                            <InputDate name="actOf" change={this.fieldChange} value={this.state.doc.actOf} />
                        </fieldset>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-6">
                        <fieldset className="form-group">
                            <label htmlFor="customerId">Customer</label>
                            <SelectEntity options={this.state.customers} change={this.fieldChange} name="customerId" required={true} value={this.state.doc.customerId} />
                        </fieldset>
                    </div>
                    <div className="col-lg-6">
                        <fieldset className="form-group">
                            <label htmlFor="accountId">Account</label>
                            <SelectEntity options={this.getAccounts()} change={this.fieldChange} name="accountId" required={true} value={this.state.doc.accountId} />
                        </fieldset>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-3">
                        <label htmlFor="num">##</label>
                        <input type="text" className="form-control" name="num" id="num" readOnly defaultValue={this.state.doc.num} />
                    </div>
                </div>

                <div className="form-group">
                    <table className="table table-sm table-items">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th width="60%">Job</th>
                                <th>Units</th>
                                <th>Price</th>
                                <th>Total</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {itemRows}
                        </tbody>
                        <tfoot>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>Total</td>
                                <td><b>{total}</b></td>
                                <td></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>

                <div className="row">
                    <div className="col-lg-6">
                        <fieldset className="form-group">
                            <label htmlFor="job">Job title</label>
                            <input type="text" className="form-control" name="job" value={this.state.item.job} onChange={this.lineChange} />
                        </fieldset>
                    </div>
                    <div className="col-lg-2">
                        <fieldset className="form-group">
                            <label htmlFor="units">Units</label>
                            <input type="text" className="form-control" name="units" value={this.state.item.units} onChange={this.lineChange} />
                        </fieldset>
                    </div>
                    <div className="col-lg-2">
                        <fieldset className="form-group">
                            <label htmlFor="price">Price</label>
                            <input type="text" className="form-control" name="price" value={this.state.item.price} onChange={this.lineChange} />
                        </fieldset>
                    </div>
                    <div className="col-lg-2">
                        <label>&nbsp;</label><br />
                        <button className="btn btn-info" onClick={this.addItem}><i className="fa fa-floppy-o"></i></button>
                        <button className="btn btn-secondary m-l-05" onClick={this.cancelItem}><i className="fa fa-ban"></i></button>
                    </div>
                </div>

                <button className="btn btn-sm btn-primary" type="submit">Save</button>
            </form>
        );
    }
});

module.exports = FormDoc;