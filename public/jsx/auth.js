var _ = require('./../../lib/_');

var token;

var auth = function(){};

auth.prototype.setToken = function(value) {
    token = value;

    return this;
};

auth.prototype.rmToken = function() {
    token = null;

    return this;
};

auth.prototype.getToken = function() {
    return token;
};

module.exports = (function(){

    var instance;

    function createInstance() {
        return new auth();
    }

    return {
        getInstance: function() {
            if(!instance) {
                instance = createInstance();
                var token = _.getCookie('token');
                if(token) {
                    instance.setToken(token);
                }
            }

            return instance;
        }
    }
})();