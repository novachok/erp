import { Router, Route, browserHistory, IndexRoute } from 'react-router'

var ReactDom   = require('react-dom');

var FormLogin          = require('./components/formLogin');
var _                  = require('./../../lib/_');
var IdentityStore      = require('./flux/store/identity');
var IdentityActions    = require('./flux/actions/identity');
var auth               = require('./auth').getInstance();
var connectClass       = require('./connect');
var NotificationAction = require('./flux/actions/notification');

var connect = new connectClass();
const rootRoute = [{
    path: '/',
    component: require('./dashboard'),
    indexRoute: { component: require('./components/moduleStats') },
    childRoutes: require('./routes')
}];


var ErpApp = React.createClass({
    displayName: 'ErpApp',
    getInitialState: function() {
        return {
            token: IdentityStore.getToken(),
            error: null
        };
    },
    componentDidMount: function(){
        IdentityStore.addChangeListener(this._onChange);

        if(auth.getToken()) {
            IdentityActions.load(auth.getToken());
        }
    },
    componentWillUnmount: function() {
        IdentityStore.removeChangeListener(this._onChange);
    },
    _onChange: function() {
        this.setState({token: IdentityStore.getToken()});
    },
    authorize: function(login, pwd, remember) {
        $.post('/api/auth', {login: login, password: pwd}, function(response){
            if(response.success) {
                _.setCookie('token', response.data.token, (remember ? 30 : false));
                auth.setToken(response.data.token);
                IdentityActions.set(response.data);
            } else {
                this.setState({error: response.error.message});
            }
        }.bind(this), 'json');
    },
    resetPassword: function(login) {
        connect.post(
            '/api/reset',
            {login: login},
            {
                lock: false
            }
        );
        NotificationAction.add('Check your e-mail, there should be a reset link', "success");
    },
    render: function(){
        if(this.state.token) {
            return React.createElement('div', {className: 'erpApp'},
                <Router history={browserHistory} routes={rootRoute}></Router>
            );
        } else {
            return (
                <FormLogin authorize={this.authorize} reset={this.resetPassword} error={this.state.error} />
            );
        }
    }
});


ReactDom.render(
    <ErpApp />,
    document.getElementById('erp-app')
);