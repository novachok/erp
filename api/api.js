var express  = require('express');
var security = require('../sys/security');
var db       = require('../sys/db');
var _        = require('../lib/_');

var app = express();

app.set('responder', function(res, next) {
    if('data' in res || 'message' in res)
    {
        var response = { success: true };
        if('data' in res) {
            response.data = res.data;
        }
        if('message' in res) {
            response.message = res.message;
        }
        res.json(response);
    } else {
        next();
    }
});


app.post('/auth', function(req, res, next){
    security
        .authorize(req.body.login, req.body.password)
        .then(function(identity){
            res.data = identity;
            return app.get('responder')(res, next);
        })
        .catch(function(err){
            return next(err);
        })
});

app.post('/reset', function(req, res, next) {
    security
        .reset(req.body.login)
        .then(function(){
            res.message = 'Instructions how to reset password have been sent to your email'
            return app.get('responder')(res, next);
        })
        .catch(function(err){
            return next(err);
        })
});


/**
 * Check if Token is valid
 */
app.use(function(req, res, next) {
    var token = req.body.token || null;
    token = req.query.token || token;

    if(!token) {
        var err = new Error(1002); // Token is expired
        next(err);
    } else {
        var Token = db.getModel('token');
        return Token.findOne({
                where: {
                    token: token
                }
            })
            .then(function(token) {
                var identity = {
                    token: token.token
                };

                return loadUser(token.userId, res, next, function(data) {
                    if(data instanceof Error) {
                        var err = new Error(1002); // Token is expired
                        return next(err);
                    } else {
                        identity.user = data.safeData;
                        req.identity = identity;
                        return next();
                    }
                });
            })
            .catch(function(){
                var err = new Error(1002);
                return next(err);
            });
    }
});


app.get('/identity', function(req, res, next) {
    res.data = req.identity;
    app.get('responder')(res, next);
});

app.put('/identity', function(req, res, next) {
    var BankAccount = db.getModel('bankAccount');

    return loadUser(req.identity.user.id, res, next, function(user) {
        var accounts2delete = [];
        for(var x in user.accounts) {
            accounts2delete.push(user.accounts[x].id.toString());
        }

        var postData = req.body;
        if('password' in postData) {
            postData.password = security.sha256(postData.password, user.salt);
        }

        return user.updateAttributes(postData)
            .then(function(user){

                for(var x in postData.accounts) {
                    if(postData.accounts[x].id < 0) {
                        postData.accounts[x].id = null;
                    } else {
                        var i = accounts2delete.indexOf(postData.accounts[x].id.toString());
                        accounts2delete.splice(i, 1);
                    }
                    postData.accounts[x].userId = user.id;
                }

                return BankAccount.bulkCreate(postData.accounts, {updateOnDuplicate: ['title', 'mfo', 'account', 'currency', 'isDefault']})
                    .then(function() {
                        res.message = 'Your profile was updated';

                        if(accounts2delete.length > 0) {
                            return BankAccount.destroy({
                                    where: {
                                        id: {$in: accounts2delete}
                                    }
                                })
                                .then(function() {
                                    return loadUser(user.id, res, next);
                                })
                                .catch(function(err) {
                                    return next(err);
                                });
                        } else {
                            return loadUser(user.id, res, next);
                        }
                    })
                    .catch(function(err) {
                        return next(err);
                    });
            })
            .catch(function(err) {
                return next(err);
            });
    });
});

app.get('/customers', function(req, res, next){
    var Customer = db.getModel('customer');
    return Customer.findAll()
        .then(function(customers) {
            res.data = customers;
            return app.get('responder')(res, next);
        })
        .catch(function(err) {
            return next(err);
        });
});

app.get('/customer/:id', function(req, res, next) {
    var Customer = db.getModel('customer');
    return Customer.findOne({
            where: {
                id: req.params.id
            }
        })
        .then(function(customer) {
            res.data = customer;
            return app.get('responder')(res, next);
        })
        .catch(function (err) {
            return next(err);
        });
});

app.post('/customer', function(req, res, next) {
    var Customer = db.getModel('customer');
    var data = req.body;
    if('id' in data) {
        delete data.id;
    }

    return Customer.build(data)
        .save()
        .then(function(customer) {
            res.data = customer;
            res.message = 'Customer was successfully created';
            return app.get('responder')(res, next);
        })
        .catch(function(err) {
            return next(err);
        });
});


app.put('/customer/:id', function(req, res, next) {
    var Customer = db.getModel('customer');
    var data = req.body;
    if('id' in data) {
        delete data.id;
    }

    return Customer.findOne({
            where: {
                id: req.params.id
            }
        })
        .then(function(customer) {
            return customer.updateAttributes(data)
                .then(function(customer){
                    res.data = customer;
                    res.message = "Customer's data was updated";
                    return app.get('responder')(res, next);
                })
                .catch(function(err) {
                    return next(err);
                })
        })
        .catch(function(err) {
            return next(err);
        });
});

app.delete('/customer/:id', function(req, res, next) {
    var Customer = db.getModel('customer');
    return Customer.destroy({
            where: {
                id: req.params.id
            }
        })
        .then(function(){
            res.message = "Customer (id: "+ req.params.id +") was removed";
            return app.get('responder')(res, next);
        })
        .catch(function(err) {
            return next(err);
        });
});

/** Hosting **/
app.get('/hosting', function(req, res, next) {
    var Hosting = db.getModel('hosting');
    var Customer = db.getModel('customer');
    return Hosting.findAll({
            include: [{
                model: Customer
            }],
            order: [['isOff', 'ASC'], ['validTo', 'ASC']]
        })
        .then(function(hosts) {
            res.data = hosts;
            return app.get('responder')(res, next);
        })
        .catch(function(err) {
            return next(err);
        });
});

app.post('/hosting', function(req, res, next) {
    var Hosting = db.getModel('hosting');
    var data = req.body;
    if('id' in data) {
        delete data.id;
    }

    return Hosting.build(data)
        .save()
        .then(function(hosting) {
            return hosting.getCustomer()
                .then(function(customer){
                    res.data = hosting.toJSON();
                    res.data.cutomer = customer.toJSON();
                    res.message = 'Hosting was successfully created';
                    return app.get('responder')(res, next);
                })
                .catch(function(err) {
                    return next(err);
                });
        })
        .catch(function(err) {
            return next(err);
        });
});

app.put('/hosting/:id', function(req, res, next) {
    var Hosting = db.getModel('hosting');
    var Customer = db.getModel('customer');

    var data = req.body;
    if('id' in data) {
        delete data.id;
    }

    return Hosting.findOne({
            where: {
                id: req.params.id
            },
            include: [{
                model: Customer
            }]
        })
        .then(function(host){
            return host.updateAttributes(data)
                .then(function(host){
                    res.data = host.toJSON();
                    return app.get('responder')(res, next);
                })
                .catch(function(err) {
                    return next(err);
                });
        })
        .catch(function(err){
            return next(err);
        });
});

app.delete('/hosting/:id', function(req, res, next) {
    var Hosting = db.getModel('hosting');
    return Hosting.destroy({
            where: {
                id: req.params.id
            }
        })
        .then(function() {
            res.message = "Hosting record (id: "+ req.params.id +") was removed";
            return app.get('responder')(res, next);
        })
        .catch(function(err) {
            return next(err);
        });
});

/** Documents */
app.get('/docs', function(req, res, next) {
    var start = 0;
    if('start' in req.query && _.isNumeric(req.query.start)) {
        start = req.query.start * 1;
    }
    var items = 5;
    if('items' in req.query && _.isNumeric(req.query.items)) {
        items = req.query.items * 1;
    }

    var Doc      = db.getModel('doc');
    var Customer = db.getModel('customer');
    var BankAccount = db.getModel('bankAccount');
    var DocItem  = db.getModel('docItem');
    var Items    = Doc.hasMany(DocItem, {as: 'items'});
    var BA = Doc.belongsTo(BankAccount, {foreignKey: 'accountId', as: 'account'});

    return Doc.findAll({
            attributes: [db.count('id')]
        })
        .then(function(count) {
            count = count[0].id;
            return Doc.findAll({
                    include: [
                        {model: Customer},
                        BA,
                        Items
                    ],
                    order: [['num', 'DESC']],
                    offset: start,
                    limit: items
                })
                .then(function(docs) {
                    var _tmp = [];
                    for(var x in docs) {
                        var _v = docs[x].dataValues;
                        _v.customer = _v.customer.dataValues;
                        _v.account  = _v.account.dataValues;
                        _tmp.push(_v)
                    }

                    res.data = {
                        total: count,
                        docs: _tmp
                    };
                    return app.get('responder')(res, next);
                })
                .catch(function(err) {
                    return next(err);
                });
        })
        .catch(function(err) {
            return next(err);
        });
});

app.post('/doc', function(req, res, next) {
    var Doc      = db.getModel('doc');
    var DocItem  = db.getModel('docItem');

    var data     = req.body;
    if('id' in data) {
        delete data.id;
    }

    return Doc.findOne({
            order: 'num DESC'
        })
        .then(function(doc){
            data.num = doc ? doc.num*1 + 1 : 1;

            return Doc.build(data)
                .save()
                .then(function(doc) {

                    if('items' in data) {
                        for (var i = 0; i < data.items.length; i++) {
                            data.items[i].id = null;
                            data.items[i].docId = doc.id;
                        }

                        return DocItem.bulkCreate(data.items)
                            .then(function(){
                                return loadDoc(doc.id, res, next);
                            });
                    } else {
                        return loadDoc(doc.id, res, next);
                    }
                })
                .catch(function(err) {
                    return next(err);
                });
        })
        .catch(function(err){
            return next(err);
        });
});

app.put('/doc/:id', function(req, res, next) {
    var DocItem  = db.getModel('docItem');

    var data = req.body;
    if('id' in data) {
        delete data.id;
    }

    return loadDoc(req.params.id, res, next, function(doc) {

        var itemsId2del = [];
        for(var x in doc.items) {
            itemsId2del.push(doc.items[x].id.toString());
        }

        return doc.updateAttributes(data)
            .then(function(doc) {

                for(var x in data.items) {
                    if(data.items[x].id < 0) {
                        data.items[x].id = null;
                    } else {
                        var i = itemsId2del.indexOf(data.items[x].id.toString());

                        if (i > -1) {
                            itemsId2del.splice(i, 1);
                        }
                    }
                    data.items[x].docId = doc.id;
                }

                return DocItem.bulkCreate(data.items, {updateOnDuplicate: ['job', 'units', 'price']})
                    .then(function() {
                        if(itemsId2del.length > 0) {
                            return DocItem.destroy({
                                    where: {
                                        id: {$in: itemsId2del}
                                    }
                                })
                                .then(function() {
                                    return loadDoc(doc.id, res, next);
                                })
                                .catch(function(err) {
                                    return next(err);
                                });
                        } else {
                            return loadDoc(doc.id, res, next);
                        }
                    })
                    .catch(function(err) {
                        return next(err);
                    });
            })
            .catch(function(err) {
                return next(err);
            });
    });
});



function loadDoc(id, res, next, callback) {
    var Doc      = db.getModel('doc');
    var Customer = db.getModel('customer');
    var DocItem  = db.getModel('docItem');
    var Items    = Doc.hasMany(DocItem, {as: 'items'});

    return Doc.findOne({
            where: {
                id: id
            },
            include: [
                {model: Customer},
                Items
            ]
        })
        .then(function(doc) {
            if(callback) {
                return callback(doc);
            } else {
                res.data = doc.toJSON();
                return app.get('responder')(res, next);
            }
        })
        .catch(function(err) {
            return next(err);
        });
}

function loadUser(id, res, next, callback) {
    var User        = db.getModel('user');
    var BankAccount = db.getModel('bankAccount');
    var Accounts    = User.hasMany(BankAccount, {as: 'accounts'});

    return User.findOne({
            where: {
                id: id
            },
            include: [Accounts]
        })
        .then(function(user) {
            if(callback) {
                return callback(user);
            } else {
                res.data = user.safeData;
                return app.get('responder')(res, next);
            }
        })
        .catch(function(err) {
            if(callback) {
                return callback(err);
            } else {
                return next(err);
            }
        });
}


module.exports = app;